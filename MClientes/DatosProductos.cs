﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace MClientes
{
    class DatosProductos
    {

        String cn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        private MySqlConnection cnn;
        public DatosProductos()
        {
            cnn = new MySqlConnection(cn);
        }

        public List<Producto> ObtenerProducto()
        {
            var lista = new List<Producto>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from productos";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unProducto = new Producto();
                        unProducto.idproducto = Convert.ToInt32(reader["idproducto"]);
                        unProducto.codigo_bar = Convert.ToString(reader["codigo_bar"]);
                        unProducto.codigo = Convert.ToString(reader["codigo"]);
                        unProducto.codigo_fab = Convert.ToString(reader["codigo_fab"]);
                        unProducto.articulo = Convert.ToString(reader["articulo"]);
                        unProducto.precio_con = Convert.ToDouble(reader["precio_con"]);
                        unProducto.precio_cos = Convert.ToDouble(reader["precio_cos"]);
                        unProducto.unimedida= Convert.ToString(reader["unimedida"]);
                        unProducto.proveedor = Convert.ToString(reader["proveedor"]);
                        unProducto.clase = Convert.ToString(reader["clase"]);
                        unProducto.pais = Convert.ToString(reader["pais"]);
                        unProducto.subclase = Convert.ToString(reader["subclase"]);
                        unProducto.modelo = Convert.ToString(reader["modelo"]);
                        unProducto.moneda = Convert.ToString(reader["moneda"]);
                        unProducto.utilidad = Convert.ToDecimal(reader["utilidad"]);
                        unProducto.marca = Convert.ToString(reader["marca"]);
                        unProducto.foto = Convert.ToString(reader["foto"]);
                        unProducto.stock = Convert.ToInt64(reader["stock"]);

                        lista.Add(unProducto);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }
        public int guardarproducto(Producto prod)
        {
           
            string query = "INSERT INTO productos (codigo,codigo_bar,codigo_fab,articulo,precio_cos,"+
                "unimedida,precio_con,proveedor,clase,pais,subclase,modelo,moneda,utilidad,marca,foto,stock) values("
                + "'" + prod.codigo + "','" + prod.codigo_bar + "','" + prod.codigo_fab + "','" + prod.articulo 
                + "','" + prod.precio_cos + "','" + prod.unimedida +
                "','" + prod.precio_con + "','" + prod.proveedor + "','" + prod.clase + "','" + prod.pais+ "','" + prod.subclase+
                 "','" + prod.modelo + "','" + prod.moneda+"','"+prod.utilidad +"','"+prod.marca+"','"+prod.foto+"',"+prod.stock+")";

            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            int insertar;
            cnn.Open();
            insertar = cmd.ExecuteNonQuery();
            cmd.CommandText = "SELECT LAST_INSERT_ID()";
           int ultimoid = Convert.ToInt32(cmd.ExecuteScalar());
            cnn.Close();
             return ultimoid;
        }
        public void eliminarProducto(long id)
        {

            string conexion = "DELETE FROM productos where idproducto=" + id;

            MySqlCommand cmd = new MySqlCommand(conexion, cnn);
            cmd.CommandType = CommandType.Text;
            cnn.Open();
            int eliminar = cmd.ExecuteNonQuery();
            cnn.Close();

        }

        public void ModificarProducto(Producto prod)
        {

            string query = "UPDATE productos SET codigo=" + "'" + prod.codigo + "' , codigo_bar= '" + prod.codigo_bar+
                "', codigo_fab='" + prod.codigo_fab+ "', articulo= '"+prod.articulo+ "',precio_cos=" +prod.precio_cos +
                ", unimedida='" + prod.unimedida +"', precio_con=" + prod.precio_con + ", proveedor='" + prod.proveedor +
                "', clase='" + prod.clase +"',pais='"+prod.pais+"',subclase='"+prod.subclase+"',modelo='"+prod.modelo+
                "',moneda='"+prod.moneda+"',marca='"+prod.marca+ "',utilidad="+prod.utilidad +",foto='"+ prod.foto+"',stock="+prod.stock +"  where idproducto= " + prod.idproducto;
            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            cnn.Open();
            int editar = Convert.ToInt16(cmd.ExecuteNonQuery());
            cnn.Close();
        }

        public List<Producto> filtrarProducto(String producto)
        {
            var lista = new List<Producto>();
            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;
                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "SELECT * FROM productos WHERE (articulo like '%"+producto+"%' OR codigo='"+producto+"')";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unProducto = new Producto();
                        unProducto.idproducto = Convert.ToInt32(reader["idproducto"]);
                        unProducto.codigo_bar = Convert.ToString(reader["codigo_bar"]);
                        unProducto.codigo = Convert.ToString(reader["codigo"]);
                        unProducto.codigo_fab = Convert.ToString(reader["codigo_fab"]);
                        unProducto.articulo = Convert.ToString(reader["articulo"]);
                        unProducto.precio_con = Convert.ToDouble(reader["precio_con"]);
                        unProducto.precio_cos = Convert.ToDouble(reader["precio_cos"]);
                        unProducto.unimedida = Convert.ToString(reader["unimedida"]);
                        unProducto.proveedor = Convert.ToString(reader["proveedor"]);
                        unProducto.clase = Convert.ToString(reader["clase"]);
                        unProducto.pais = Convert.ToString(reader["pais"]);
                        unProducto.subclase = Convert.ToString(reader["subclase"]);
                        unProducto.modelo = Convert.ToString(reader["modelo"]);
                        unProducto.moneda = Convert.ToString(reader["moneda"]);
                        unProducto.utilidad = Convert.ToDecimal(reader["utilidad"]);
                        unProducto.marca = Convert.ToString(reader["marca"]);
                        unProducto.foto = Convert.ToString(reader["foto"]);
                        unProducto.stock = Convert.ToInt64(reader["stock"]);
                        lista.Add(unProducto);
                    }
                    reader.Close();
                    cnx.Close();
                }
            }
            return lista;
        }

    }
}
