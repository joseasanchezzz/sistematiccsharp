﻿using System;
using BasicLibrary.Components;
//using DataAccess.Nucleo.Dominio;
//using DataAccess.Persistencia;
using System.Collections.Generic;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Windows.Forms;

namespace MClientes
{
    public sealed class AuxiliarDatos
    {
        
        String cn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        private MySqlConnection cnn;
        public static AuxiliarDatos InstanciaUnica { get; } = new AuxiliarDatos();

        public SortableBindingList<Producto> ColeccionProductos { get; }
        //public SortableBindingList<ventas> ColeccionVentas { get; }
        public SortableBindingList<Detalle_com> ColeccionDetalles { get; }

        public IList<string> ColeccionCodigos { get; }


        //public Dictionary<string, string> TiposDoc { get; private set; }

        //public string CodigoTipoDoc { get; set; }

        //private ventas ventaActual;
        //public ventas VentaActual
        //{
        //    get { return ventaActual; }

        //    set
        //    {
        //        ventaActual = value;

        //        VentaActualModificada?.Invoke(this, EventArgs.Empty);
        //    }
        //}

        //public string Serie { get; } = "0001";




        public event EventHandler<EventArgs> VentaActualModificada;

        /// <summary>
        /// Inicializa una nueva instancia de la clase AuxiliarDatos.
        /// </summary>
        private AuxiliarDatos()
        {
            ColeccionProductos = new SortableBindingList<Producto>();
            //ColeccionVentas = new SortableBindingList<ventas>();
            ColeccionDetalles = new SortableBindingList<Detalle_com>();
            ColeccionCodigos = new List<string>();
            cnn = new MySqlConnection(cn);
        }

    



        //public event EventHandler<EventArgs> ColeccionVentasCambiada;

        //private void ColeccionVentas_ListChanged(object sender, System.ComponentModel.ListChangedEventArgs e)
        //{
        //    ColeccionVentasCambiada?.Invoke(this, EventArgs.Empty);
        //}

        //// la serie esta en la clase actual
        //public string ObtenerDocumentoSiguiente()
        //{
        //    int correlativo;

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        correlativo = unidad.Serie.ObtenerCorrelativoPorTipoYSerie(CodigoTipoDoc, Serie);
        //    }

        //    return (correlativo == 0) ? null
        //                              //: $"{CodigoTipoDoc} {Serie}-{correlativo:D8}";
        //                              : $"{Serie}-{correlativo:D8}";
        //}





        //public void ObtenerVentas()
        //{
        //    ColeccionVentas.Clear();

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Venta.
        //            ObtenerTodos().
        //            OrderByDescending(x => x.fecha).
        //            ToList().
        //            ForEach(x => ColeccionVentas.Add(x));
        //    }
        //}

        public void ObtenerProductos()
        {
            ColeccionProductos.Clear();
            ColeccionCodigos.Clear();

            //using (var unidad = new UnidadDeTrabajo())
            //{
               var productos = obtenerProductoBd();

            foreach (var producto in productos)
            {
                ColeccionProductos.Add(producto);
                ColeccionCodigos.Add(producto.codigo);
            }
            //}
        }

        public List<Producto> obtenerProductoBd()
    {
        var lista = new List<Producto>();

        using (var cnx = new MySqlConnection())
        {
            cnx.ConnectionString = cn;

            using (var cmd = new MySqlCommand())
            {
                cmd.CommandText = "select * from productos";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cnx;
                cnx.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    var unProducto = new Producto();
                    unProducto.idproducto = Convert.ToInt32(reader["idproducto"]);
                    unProducto.codigo_bar = Convert.ToString(reader["codigo_bar"]);
                    unProducto.codigo = Convert.ToString(reader["codigo"]);
                    unProducto.codigo_fab = Convert.ToString(reader["codigo_fab"]);
                    unProducto.articulo = Convert.ToString(reader["articulo"]);
                    unProducto.precio_con = Convert.ToDouble(reader["precio_con"]);
                    unProducto.precio_cos = Convert.ToDouble(reader["precio_cos"]);
                    unProducto.unimedida = Convert.ToString(reader["unimedida"]);
                    unProducto.proveedor = Convert.ToString(reader["proveedor"]);
                    unProducto.clase = Convert.ToString(reader["clase"]);
                    unProducto.pais = Convert.ToString(reader["pais"]);
                    unProducto.subclase = Convert.ToString(reader["subclase"]);
                    unProducto.modelo = Convert.ToString(reader["modelo"]);
                    unProducto.moneda = Convert.ToString(reader["moneda"]);
                    unProducto.utilidad = Convert.ToDecimal(reader["utilidad"]);
                    unProducto.marca = Convert.ToString(reader["marca"]);
                    unProducto.foto = Convert.ToString(reader["foto"]);
                    unProducto.stock = Convert.ToInt64(reader["stock"]);

                    lista.Add(unProducto);
                }
                reader.Close();

                cnx.Close();
            }
        }

        return lista;
    }

        public List<Proveedores> obtenerProveedorBd()
        {
            var lista = new List<Proveedores>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from proveedores";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unProveedor = new Proveedores();
                        unProveedor.idproveedor = Convert.ToInt32(reader["idproveedor"]);
                        unProveedor.proveedor = Convert.ToString(reader["proveedor"]);
                        //unProducto.codigo = Convert.ToString(reader["codigo"]);
                        //unProducto.codigo_fab = Convert.ToString(reader["codigo_fab"]);
                        //unProducto.articulo = Convert.ToString(reader["articulo"]);
                        //unProducto.precio_con = Convert.ToDouble(reader["precio_con"]);
                        //unProducto.precio_cos = Convert.ToDouble(reader["precio_cos"]);
                        //unProducto.unimedida = Convert.ToString(reader["unimedida"]);
                        //unProducto.proveedor = Convert.ToString(reader["proveedor"]);
                        //unProducto.clase = Convert.ToString(reader["clase"]);
                        //unProducto.pais = Convert.ToString(reader["pais"]);
                        //unProducto.subclase = Convert.ToString(reader["subclase"]);
                        //unProducto.modelo = Convert.ToString(reader["modelo"]);
                        //unProducto.moneda = Convert.ToString(reader["moneda"]);
                        //unProducto.utilidad = Convert.ToDecimal(reader["utilidad"]);
                        //unProducto.marca = Convert.ToString(reader["marca"]);
                        //unProducto.foto = Convert.ToString(reader["foto"]);
                        //unProducto.stock = Convert.ToInt64(reader["stock"]);

                        lista.Add(unProveedor);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }

        public int guardarCabecera(Compra com)
        {

            string query = "INSERT INTO compras (documento,idproveedor,fecha,subtotal,igv,total,tipodoc) values("
                + "'" + com.documento + "','" + com.idproveedor + "','" + com.fecha.Value.ToString("yyyy-MM-dd HH:mm:ss") + "','" + com.subtotal
                + "','" +com.igv + "','" + com.total +"','"+com.tipodoc+"')";
            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            int insertar;
            cnn.Open();
            insertar = cmd.ExecuteNonQuery();
            cmd.CommandText = "SELECT LAST_INSERT_ID()";
            int ultimoid = Convert.ToInt32(cmd.ExecuteScalar());
            cnn.Close();
            return ultimoid;
        }

        public void guardarDetalle_com(Detalle_com dta_com)
        {

            string query = "INSERT INTO detalles_com (idfactura,idproducto,codigo,articulo,cantidad,precio,valor_vta,unimedida) values("
                + "'" + dta_com.idfactura + "','" + dta_com.idproducto + "','" + dta_com.codigo + "','" + dta_com.articulo
                + "','" + dta_com.cantidad + "','" + dta_com.precio + "','"+ dta_com.valor_vta+"','"+dta_com.unimedida+"')";
            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            int insertar;
            cnn.Open();
            insertar = cmd.ExecuteNonQuery();
            cmd.CommandText = "SELECT LAST_INSERT_ID()";
            int ultimoid = Convert.ToInt32(cmd.ExecuteScalar());
            cnn.Close();
           // return ultimoid;
        }



        //public void SeleccionarVentaPorId(long idVenta)
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        VentaActual = unidad.Venta.ObtenerPorId(idVenta);
        //    }

        //    if (VentaActual == null)
        //    {
        //        return;
        //    }

        //    //VentaActual.usuario = "CAJERO";
        //    ObtenerDetallesPorIdVenta(idVenta);
        //}

        //public void ObtenerDetallesPorIdVenta(long idVenta)
        //{
        //    ColeccionDetalles.Clear();

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.DetalleVenta.
        //            ObtenerSegunCondicion(x => x.idfactura == idVenta).
        //            ToList().
        //            ForEach(x => ColeccionDetalles.Add(x));
        //    }
        //}

        ////public string ObtenerDocumentoSiguiente()
        ////{
        ////    using (var unidad = new UnidadDeTrabajo())
        ////    {
        ////        return (unidad.Venta.ObtenerDocumentoSiguiente());
        ////    }
        ////}












        //public void InsertarNuevaVenta(ventas nuevaVenta)
        //{
        //    // Inserta en la tabla "caja"
        //    long idNuevaCaja = InsertarCaja(nuevaVenta);

        //    // Inserta en la tabla "ventas" y "detalles_vta"
        //    // Antes de insertar se asigna el idcaja de la fila recién insertada
        //    nuevaVenta.idcaja = Convert.ToString(idNuevaCaja);
        //    InsertarVenta(nuevaVenta);

        //    // Inserta en la tabla "kardex"
        //    InsertarKardex(nuevaVenta);

        //    // Guarda el código del documento para que incremente el correlativo
        //    GuardarCodigoDocumento();
        //}

        //private long InsertarCaja(ventas unaVenta)
        //{
        //    var nuevaCaja = new caja();

        //    nuevaCaja.documento      = $"{CodigoTipoDoc}.{unaVenta.documento}";
        //    nuevaCaja.fecha          = DateTime.Now;
        //    nuevaCaja.concepto       = "VENTA";
        //    nuevaCaja.entrada        = unaVenta.total;
        //    nuevaCaja.salida         = 0;
        //    nuevaCaja.moneda         = "S/";
        //    nuevaCaja.turno          = null;
        //    nuevaCaja.local          = 1;
        //    nuevaCaja.num_caja       = 1;
        //    nuevaCaja.usuario        = UsuarioActual.login;
        //    nuevaCaja.destino_origen = "";
        //    nuevaCaja.tarjeta        = "";
        //    nuevaCaja.ref_tarjeta    = "";

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Caja.Insertar(nuevaCaja);
        //        unidad.Finalizar();

        //        return nuevaCaja.id;
        //    }
        //}

        //private void InsertarVenta(ventas nuevaVenta)
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Venta.Insertar(nuevaVenta);
        //        unidad.Finalizar();
        //    }

        //    // Agrega la nueva venta al inicio de la colección
        //    ColeccionVentas.Insert(0, nuevaVenta);

        //    // Asigna el id de la nueva venta a cada elemento de la colección de detalles_vta
        //    foreach (var detalle in ColeccionDetalles)
        //    {
        //        detalle.idfactura = nuevaVenta.idfactura;
        //    }

        //    // Inserta los detalles de la venta desde ColeccionDetalles
        //    InsertarDetallesVenta(ColeccionDetalles);
        //}

        //private static void InsertarDetallesVenta(IEnumerable<detalles_vta> detalles)
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        foreach (var detalle in detalles)
        //        {
        //            unidad.DetalleVenta.Insertar(detalle);
        //        }

        //        unidad.Finalizar();
        //    }
        //}

        //private void InsertarKardex(ventas unaventa)
        //{
        //    // Crea una lista del tamaño de la colección de detalles
        //    var coleccionKardex = new List<kardex>(ColeccionDetalles.Count);

        //    // Por cada detalles se crea una fila para la tabla kardex
        //    foreach (var detalle in ColeccionDetalles)
        //    {
        //        var entradaKardex = new kardex();

        //        entradaKardex.num_kardex  = $"{unaventa.tipodoc}.{unaventa.idfactura}";
        //        entradaKardex.idproducto  = Convert.ToInt64(detalle.idproducto);
        //        entradaKardex.idfactura   = Convert.ToString(detalle.idfactura);
        //        entradaKardex.documento   = $"{unaventa.tipodoc}.{unaventa.documento}";
        //        entradaKardex.fecha       = DateTime.Now;
        //        entradaKardex.responsable = unaventa.usuario;
        //        entradaKardex.referencia  = null;
        //        entradaKardex.precio      = (double)detalle.precio;
        //        entradaKardex.entrada     = 0;
        //        entradaKardex.salida      = (double)detalle.cantidad;
        //        entradaKardex.valor_vta   = (double)detalle.valor_vta;
        //        entradaKardex.stock_ant   = null;
        //        entradaKardex.stock_act   = null;
        //        entradaKardex.observacion = "";
        //        entradaKardex.almacen     = 1;
        //        entradaKardex.val_existe  = null;
        //        entradaKardex.cantidad2   = 0;

        //        coleccionKardex.Add(entradaKardex);
        //    }

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Kardex.InsertarColeccion(coleccionKardex);
        //        unidad.Finalizar();
        //    }
        //}

        //private void GuardarCodigoDocumento()
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Serie.IncrementarCorrelativoPorTipoYSerie(CodigoTipoDoc, Serie);
        //        unidad.Finalizar();
        //    }
        //}



        //public void ModificarVenta(ventas ventaModificada)
        //{
        //    // Asigna el id de la venta actual a la venta que contiene los datos modificados
        //    ventaModificada.idfactura = VentaActual.idfactura;

        //    foreach (var detalle in ColeccionDetalles)
        //    {
        //        detalle.idfactura = VentaActual.idfactura;
        //    }

        //    //CAJA
        //    // Modificar caja
        //    string codigoDocumento = $"{ventaModificada.tipodoc}.{ventaModificada.documento}";

        //    // Elimina la fila de la tabla caja que corresponde a la venta que se va a modificar
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Caja.EliminarCajaPorDocumento(codigoDocumento);
        //        unidad.Finalizar();
        //    }

        //    // Luego inserta una nueva con los datos de la venta modificada
        //    long idCaja = InsertarCaja(ventaModificada);
        //    ventaModificada.idcaja = Convert.ToString(idCaja);



        //    // DETALLES_VTA
        //    // Se eliminan los detalles de la venta
        //    EliminarDetallesVentaPorId(ventaModificada.idfactura);

        //    // Se insertan los nuevos
        //    InsertarDetallesVenta(ColeccionDetalles);


        //    // VENTAS
        //    //Modificar
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Venta.ModificarVenta(ventaModificada);
        //        unidad.Finalizar();
        //    }


        //    // KARDEX
        //    // Se eliminan los kardex
        //    EliminarKardexPorIdVenta(ventaModificada.idfactura);

        //    // Se vuelven a insertar
        //    InsertarKardex(ventaModificada);



        //    // Se actualiza en ColeccionVentas, para actualizar sus datos
        //    var posicion = ColeccionVentas.IndexOf(ColeccionVentas.SingleOrDefault(x => x.idfactura == ventaModificada.idfactura));
        //    ColeccionVentas[posicion] = ventaModificada;
        //}

        //public void EliminarVentaPorId(long idVenta)
        //{
        //    var venta = ColeccionVentas.SingleOrDefault(x => x.idfactura == idVenta);

        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Venta.Eliminar(venta);
        //        unidad.Finalizar();
        //    }

        //    ColeccionVentas.Remove(venta);
        //}

        //public void EliminarDetallesVentaPorId(long idVenta)
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.DetalleVenta.EliminarDeatllesPorIdVenta(idVenta);
        //        unidad.Finalizar();
        //    }
        //}

        //public void EliminarKardexPorIdVenta(long idVenta)
        //{
        //    using (var unidad = new UnidadDeTrabajo())
        //    {
        //        unidad.Kardex.EliminarKardexPorIdVenta(idVenta);
        //        unidad.Finalizar();
        //    }
        //}
    }
}