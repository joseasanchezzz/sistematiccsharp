﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
   public class Cliente
    {
        
        public long idcliente { get; set; }
        public string codigo { get; set; }
        public string cliente { get; set; }
        public string contacto { get; set; }
        public string ruc { get; set; }
        public string dni { get; set; }
        public string giro { get; set; }
        public string direccion { get; set; }
        public string ciudad { get; set; }
        public string ruta { get; set; }
        public string zona { get; set; }
        public string ubigeo { get; set; }
        public string telefono1 { get; set; }
        public string telefono2 { get; set; }
        public string celular { get; set; }
        public string email { get; set; }
        public string productos { get; set; }
        public Nullable<double> credito { get; set; }
        public Nullable<int> lista { get; set; }
        public string direccion2 { get; set; }
        public Nullable<System.DateTime> fecha_reg { get; set; }
        public string vendedor { get; set; }
        public long puntos { get; set; }
        public string pais { get; set; }
        public string origen { get; set; }
        public string region { get; set; }
        public Nullable<int> dia { get; set; }
        public string vehiculo { get; set; }
        public string placa { get; set; }
        public string canal { get; set; }
        public long idcontacto { get; set; }
        public string estado { get; set; }
        public int ciclo { get; set; }
        public System.DateTime fecha_cum { get; set; }
    }
}

