﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
    public class datosProveedor
    {

        String cn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        private MySqlConnection cnn;
        public datosProveedor()
        {
            cnn = new MySqlConnection(cn);
        }

        public int guardarproveedor(Proveedores prove)
        {

            string query = "INSERT INTO proveedores (codigo,proveedor,contacto,ruc,direccion,telefono1,telefono2,celular,email," +
             "  productos,giro,ciudad,ruta) values("
                + "'" + prove.codigo + "','" + prove.proveedor + "','" + prove.contacto + "','" + prove.ruc
                + "','" + prove.direccion + "','" + prove.telefono1 +
                "','" + prove.telefono2 + "','" + prove.celular + "','" + prove.email + "','" + prove.productos + "','" + prove.giro +
                 "','" + prove.ciudad + "','" + prove.ruta + "')";

            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            int insertar;
            cnn.Open();
            insertar = cmd.ExecuteNonQuery();
            cmd.CommandText = "SELECT LAST_INSERT_ID()";
            int ultimoid = Convert.ToInt32(cmd.ExecuteScalar());
            cnn.Close();
            return ultimoid;
        }

        public List<Proveedores> ObtenerProveedor()
        {
            var lista = new List<Proveedores>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from proveedores";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unProveedor = new Proveedores();
                        unProveedor.idproveedor = Convert.ToInt32(reader["idproveedor"]);
                        unProveedor.codigo = Convert.ToString(reader["codigo"]);
                        unProveedor.proveedor = Convert.ToString(reader["proveedor"]);
                        unProveedor.contacto = Convert.ToString(reader["contacto"]);
                        unProveedor.ruc = Convert.ToString(reader["ruc"]);
                        unProveedor.direccion = Convert.ToString(reader["direccion"]);
                        unProveedor.telefono1 = Convert.ToString(reader["telefono1"]);
                        unProveedor.telefono2 = Convert.ToString(reader["telefono2"]);
                        unProveedor.celular = Convert.ToString(reader["celular"]);
                        unProveedor.email = Convert.ToString(reader["email"]);
                        unProveedor.productos = Convert.ToString(reader["productos"]);
                        unProveedor.giro = Convert.ToString(reader["giro"]);
                        unProveedor.ciudad = Convert.ToString(reader["ciudad"]);
                        unProveedor.ruta = Convert.ToString(reader["ruta"]);
                        lista.Add(unProveedor);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }

        public void ModificarProveedor(Proveedores prove)
        {

            string query = "UPDATE proveedores SET proveedor=" + "'" + prove.proveedor + "' , contacto= '" + prove.contacto 
                + "', ruc='" + prove.ruc + "', direccion= '"
                + prove.direccion + "', telefono1='" + prove.telefono1 + "', telefono2='" + prove.telefono2 + "', ciudad='"+prove.ciudad 
                +"', celular='" +prove.celular + "', email='" + prove.email +
                "',giro='"+prove.giro+"', productos='"+prove.productos+"' ,ruta='" + prove.ruta + "',codigo='"+prove.codigo+"'  where idproveedor= " + prove.idproveedor;
            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            cnn.Open();
            int editar = Convert.ToInt16(cmd.ExecuteNonQuery());
            cnn.Close();
        }

        public List<Proveedores> filtrarProveedores(String cliente)
        {
            var lista = new List<Proveedores>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {

                    cmd.CommandText = "SELECT * FROM proveedores WHERE proveedor like  '%" + cliente + "%'";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unproveedor = new Proveedores();
                        unproveedor.idproveedor = Convert.ToInt32(reader["idproveedor"]);
                        unproveedor.proveedor= Convert.ToString(reader["proveedor"]);
                        unproveedor.direccion= Convert.ToString(reader["direccion"]);
                        unproveedor.codigo= Convert.ToString(reader["codigo"]);
                        unproveedor.ruc = Convert.ToString(reader["ruc"]);
                        unproveedor.contacto= Convert.ToString(reader["contacto"]);
                        unproveedor.email = Convert.ToString(reader["email"]);
                        unproveedor.ciudad= Convert.ToString(reader["ciudad"]);
                        unproveedor.celular= Convert.ToString(reader["celular"]);
                        unproveedor.telefono1 = Convert.ToString(reader["telefono1"]);
                        unproveedor.telefono2= Convert.ToString(reader["telefono2"]);
                        unproveedor.giro = Convert.ToString(reader["giro"]);
                        unproveedor.ruta = Convert.ToString(reader["ruta"]);
                        unproveedor.productos = Convert.ToString(reader["productos"]);
                        lista.Add(unproveedor);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }

        public void eliminarProveedor(long id)
        {

            string conexion = "DELETE FROM proveedores where idproveedor=" + id;

            MySqlCommand cmd = new MySqlCommand(conexion, cnn);
            cmd.CommandType = CommandType.Text;
            cnn.Open();
            int eliminar = cmd.ExecuteNonQuery();
            cnn.Close();

        }

    }
}
