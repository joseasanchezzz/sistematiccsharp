﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MClientes
{
    public partial class FRM_Clientes : Form
    {
        private BindingList<Cliente> coleccionClientes;
        private BindingList<Cliente> coleccionresult;
       

        //DataTable tablaClientes;
        DatosCliente dat = new DatosCliente();
        public FRM_Clientes()
        {
            ///coleccionClientes = new
            InitializeComponent();
            coleccionClientes = new BindingList<Cliente>(dat.ObtenerClientes());
            coleccionresult = new BindingList<Cliente>();
            // int idactual = coleccionClientes.Count;
            //tablaClientes = dat.todos();
            lbllistanombre.DataSource = coleccionClientes;
            //lbllistanombre.DataSource = tablaClientes;
            lbllistanombre.DisplayMember = "cliente";
            lbllistanombre.ValueMember = "idcliente";
            inicampos();
            
        }

        public void obtenerdatos()
        {
            blockCampos();
            var cliSel = obtenerdatosid();
            txtid.Text = Convert.ToString(cliSel.idcliente);
            txtcliente.Text = cliSel.cliente;
            txtdireccion.Text = cliSel.direccion;
            txtruc.Text = cliSel.ruc;
            txttelefono1.Text = cliSel.telefono1;
            txttelefono2.Text = cliSel.telefono2;
            //txtid.Text = Convert.ToString(cliSel.idcliente);
            txtemail.Text = cliSel.email;
            txtcelular.Text = cliSel.celular;
            txtcontacto.Text = cliSel.contacto;
            txtdireccion2.Text = cliSel.direccion2;
            btneliminar.Enabled = true;
            //btnnuevo.Enabled = false;
            btnmodificar.Enabled = true;
            btnnuevo.Visible = true;
            btnguardar.Visible = false;
            btncancelar.Visible = false;
            btnImprimir.Enabled = true;
           // activarCampo();
           



        }

        public Cliente obtenerdatosid()
        {
            if (lbllistanombre.SelectedValue == null)
            {
                MessageBox.Show("Seleccione un Cliente, por favor");
                return null;
            }
            int idSeleccionado = Convert.ToInt32(lbllistanombre.SelectedValue);
            var cliSel = coleccionClientes.SingleOrDefault(x => x.idcliente == idSeleccionado);
            if (cliSel == null)
            {
                return null;
            }
            return cliSel;
        }

        public void inicampos()
        {
            btneliminar.Enabled = false;
            btnguardar.Visible = false;
            btnmodificar.Enabled = false;
            btnImprimir.Enabled = false;
            btncancelar.Visible = false;
            txtruc.Enabled = false;
            txtcliente.Enabled = false;
            txtdireccion.Enabled = false;
            txtcontacto.Enabled = false;
            txtemail.Enabled = false;
            txtdireccion2.Enabled = false;
            txtcelular.Enabled = false;
            txttelefono1.Enabled = false;
            txttelefono2.Enabled = false;
            txtnombre.Focus();
            btnguardarmodificar.Visible = false;
        }
       

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            limpiarCampos();
            long ultimoid;
            if (coleccionClientes.Count==0) { 
          ultimoid=  1;
            } else { 
            ultimoid = coleccionClientes.Max(x => x.idcliente) + 1;
            }
            txtid.Text =Convert.ToString( ultimoid);
            btnguardar.Visible = true;
            btnnuevo.Visible = false;
            txtruc.Focus();
            activarCampo();
            btncancelar.Visible = true;
            btnImprimir.Enabled = false;
            txtnombre.Enabled = false;
            lbllistanombre.Enabled = false;
            btnregresar.Enabled = false;
        }
        public void activarCampo()
        {
            txtruc.Enabled = true;
            txtcliente.Enabled = true;
            txtdireccion.Enabled = true;
            txtcontacto.Enabled = true;
            txtemail.Enabled = true;
            txtdireccion2.Enabled = true;
            txtcelular.Enabled = true;
            txttelefono1.Enabled = true;
            txttelefono2.Enabled = true;
            btnImprimir.Enabled = true;
            txtruc.Focus();
           
        }
        public void blockCampos()
        {
            txtruc.Enabled = false;
            txtcliente.Enabled = false;
            txtdireccion.Enabled = false;
            txtcontacto.Enabled = false;
            txtemail.Enabled = false;
            txtdireccion2.Enabled = false;
            txtcelular.Enabled = false;
            txttelefono1.Enabled = false;
            txttelefono2.Enabled = false;
        }

        private void lbllistanombre_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            obtenerdatos();
        }


        public void limpiarCampos()
        {
            txtruc.Text = "";
            txtcliente.Text = "";
            txtdireccion.Text = "";
            txtcontacto.Text = "";
            txtemail.Text = "";
            txtdireccion2.Text = "";
            txtcelular.Text = "";
            txttelefono1.Text = "";
            txttelefono2.Text = "";
            txtid.Text = "";
            txtnombre.Text = "";
            //  txtid.Text = Convert.ToString(idactual);
            btneliminar.Enabled = false;
            btnmodificar.Enabled = false;
            btnguardar.Visible = false;
            btnnuevo.Enabled = true;
            btnnuevo.Visible = true;
            btncancelar.Visible = false;
            btnImprimir.Enabled = false;

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            //limpiarCampos();
              List<Cliente> listcliente = new List<Cliente>(1);
            listcliente.Add(obtenerDatosForm());
            FRM_Reporte f = new FRM_Reporte();
         f.MostrarVistaPrevia(listcliente, "");
            f.ShowDialog(this);
            //DialogResult resul = MessageBox.Show("En construccion", "Mantenimiento", MessageBoxButtons.OK);
        }
        private List<Cliente> obtenerdatospdf()
        {
            List<Cliente> lista = new List<Cliente>();
            lista.Add(obtenerDatosForm());
            return lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (validarCampos()) { 
            var cliente = obtenerDatosForm();
            long lastid= dat.guardarcliente(cliente);
            cliente.idcliente = lastid;
            coleccionClientes.Add(cliente);
                //limpiarCampos();
                btneliminar.Enabled = false;
                btnmodificar.Enabled = false;
                btnguardar.Visible = false;
                btnnuevo.Enabled = true;
                btnnuevo.Visible = true;
                btncancelar.Visible = false;
                blockCampos();
                txtnombre.Enabled = true;
                lbllistanombre.Enabled = true;
                btnregresar.Enabled = true;
            }
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            DialogResult resul = MessageBox.Show("Seguro que quieres eliminar el cliente " + txtcliente.Text+"?", "Eliminar Registro", MessageBoxButtons.YesNo);
            if (resul == DialogResult.Yes)
            {
                var cliente = obtenerdatosid();
                coleccionClientes.Remove(cliente);
                dat.eliminarCliente(cliente.idcliente);
                limpiarCampos();
            }
        }
        

        private void button5_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
          
            activarCampo();
            btnguardarmodificar.Visible = true;
            btncancelar.Visible = true;
            btnImprimir.Enabled = false;
            btneliminar.Enabled = false;
            btnregresar.Enabled = false;
            txtnombre.Enabled = false;
            lbllistanombre.Enabled = false;
            btnregresar.Enabled = false;


        }

        public Cliente obtenerDatosForm()
        {
            Cliente clie = new Cliente();
           clie.idcliente = Convert.ToUInt32(txtid.Text);
            clie.cliente = txtcliente.Text;
            clie.contacto = txtcontacto.Text;
            clie.ruc = txtruc.Text;
            clie.direccion = txtdireccion.Text;
            clie.telefono1 = txttelefono1.Text;
            clie.telefono2 = txttelefono2.Text;
            clie.celular =txtcelular.Text;
            clie.email = txtemail.Text;
            clie.direccion2 = txtdireccion2.Text;
            return clie;
        }

     
        /**/
        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
               // DialogResult resul = MessageBox.Show("Seguro que quieres eliminar este Cliente? " + txtnombre.Text, "Eliminar Registro", MessageBoxButtons.YesNo);
             if (!txtnombre.Text.Equals(""))
            {
                //DialogResult resul = MessageBox.Show("Seguro que quieres eliminar este Cliente? "+txtnombre.Text, "Eliminar Registro", MessageBoxButtons.YesNo);
                var c = txtnombre.Text.ToLower();
                    var filcliente = dat.filtrarCliente(c);
                    //var resultados = (coleccionClientes.Where(x => x.cliente.StartsWith(c)));
                    //coleccionClientes.Clear();
                    coleccionresult.Clear();
                    foreach (var item in filcliente)
                {
                    coleccionresult.Add(item);
                }
                lbllistanombre.DataSource = null;
                lbllistanombre.DataSource = coleccionresult;
                lbllistanombre.DisplayMember = "cliente";
                lbllistanombre.ValueMember = "idcliente";
                    var t = txtnombre.Text;
                    if (coleccionresult != null && coleccionresult.Count == 1 )
                    {
                        obtenerdatos();
                    }
                    }
            else if (txtnombre.Text.Equals(""))
            {
                    lbllistanombre.DataSource = null;
                    lbllistanombre.DataSource = coleccionClientes;
                    lbllistanombre.DisplayMember = "cliente";
                    lbllistanombre.ValueMember = "idcliente";
                    DialogResult resul = MessageBox.Show("Ingrese datos a buscar", "Mensaje", MessageBoxButtons.OK);
            }
        }
        }
         
        public Boolean validarCampos()
        {
            Boolean valor=true;
            if (txtruc.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato RUC es obligatorio", "Validar", MessageBoxButtons.OK);
                txtruc.Focus();
                valor = false;
                return valor;
            }
           else if (txtcliente.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Cliente es obligatorio", "Validar", MessageBoxButtons.OK);
                txtcliente.Focus();
                valor = false;
                return valor;
            }
           else if (txtdireccion.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Direccion es obligatorio", "Validar", MessageBoxButtons.OK);
                txtdireccion.Focus();
                valor = false;
                return valor;
            }
            return valor;
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
            inicampos();
            lbllistanombre.DataSource = null;
            lbllistanombre.DataSource = coleccionClientes;
            lbllistanombre.DisplayMember = "cliente";
            lbllistanombre.ValueMember = "idcliente";
            //btneliminar.Enabled = true;
            btnregresar.Enabled = true;
            txtnombre.Enabled = true;
            lbllistanombre.Enabled = true;
        }

        private void btnguardarmodificar_Click(object sender, EventArgs e)
        {
            var cliente = obtenerDatosForm();
            long id = cliente.idcliente;
            // ventana
            int posicion = coleccionClientes.IndexOf(coleccionClientes.SingleOrDefault(x => x.idcliente == id));
            if(coleccionClientes[posicion].ruc != cliente.ruc || coleccionClientes[posicion].cliente != cliente.cliente ||
                coleccionClientes[posicion].email != cliente.email || coleccionClientes[posicion].contacto != cliente.contacto ||
                coleccionClientes[posicion].direccion != cliente.direccion || coleccionClientes[posicion].celular != cliente.celular
                || coleccionClientes[posicion].telefono1 != cliente.telefono1 || coleccionClientes[posicion].telefono2 != cliente.telefono2)
            {
                coleccionClientes[posicion] = cliente;

                // databse                        
                dat.ModificarCliente(cliente);
               // limpiarCampos();
                blockCampos();
                btnguardarmodificar.Visible = false;
                txtnombre.Enabled = true;
                lbllistanombre.Enabled = true;
                btnregresar.Enabled = true;
                lbllistanombre.DataSource = null;
                lbllistanombre.DataSource = coleccionClientes;
                lbllistanombre.DisplayMember = "cliente";
                lbllistanombre.ValueMember = "idcliente";
                btncancelar.Visible = false;
                btnmodificar.Enabled = false;
                txtnombre.Text = "";


            }
            else
            {
                DialogResult resul = MessageBox.Show("Debe modificar un dato", "Mensaje", MessageBoxButtons.OK);
                txtruc.Focus();
            }
            
        }

        private void txtnombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbllistanombre_MouseClick(object sender, MouseEventArgs e)
        {
            obtenerdatos();
        }
    }  
}
