﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MClientes
{
    public partial class FRM_Productos : Form
    {

        private BindingList<Producto> coleccionProducto;
        private BindingList<Producto> coleccionresult;
        DatosProductos datp = new DatosProductos();
        public event EventHandler<EventArgs> productoseleccionado;
        String foto="";
        public FRM_Productos()
        {
            InitializeComponent();
           
            coleccionProducto = new BindingList<Producto>(datp.ObtenerProducto());
            coleccionresult = new BindingList<Producto>();
            lbllistprod.DataSource = coleccionProducto;
            lbllistprod.DisplayMember = "articulo";
            lbllistprod.ValueMember = "idproducto";
            iniciarCampos();
            //   txtultil.Text =  (CDbl(Resultado), "0.00");
            this.FormClosing += FRM_Productos_FormClosing;

        }

        private void FRM_Productos_FormClosing(object sender, FormClosingEventArgs e)
        {
            productoseleccionado?.Invoke(this, EventArgs.Empty);
        }

        public void iniciarCampos()
        {
            txtcodigo.Enabled = false;
            txtcodigobar.Enabled = false;
            txtcodigofab.Enabled = false;
            txtdescrip.Enabled = false;
            txtcosto.Enabled = false;
            cmbunid.Enabled = false;
            txtprecioven.Enabled = false;
            txtproveedor.Enabled = false;
            txtclase.Enabled = false;
            txtmarca.Enabled = false;
            txtpais.Enabled = false;
            txtsubcalse.Enabled = false;
            txtmodelo.Enabled = false;
            cmbmoneda.Enabled = false;
            txtultil.Enabled = false;
            btncancelar.Visible = false;
            btnguardarmodificar.Visible = false;
            btnguardar.Visible = false;
            txtnombrecodigo.Focus();
            btnmodificar.Enabled = false;
            btneliminar.Enabled = false;
            pb1.Enabled = false;
            cmbunid.SelectedIndex = 0;
            cmbmoneda.SelectedIndex = 0;
            txtstock.Enabled = false;


        }
        public void blockCampos()
        {
            txtcodigo.Enabled = false;
            txtcodigobar.Enabled = false;
            txtcodigofab.Enabled = false;
            txtdescrip.Enabled = false;
            txtcosto.Enabled = false;
            cmbunid.Enabled = false;
            txtprecioven.Enabled = false;
            txtproveedor.Enabled = false;
            txtclase.Enabled = false;
            txtmarca.Enabled = false;
            txtpais.Enabled = false;
            txtsubcalse.Enabled = false;
            txtmodelo.Enabled = false;
            cmbmoneda.Enabled = false;
            txtultil.Enabled = false;
            btncancelar.Visible = false;
            btnguardar.Visible = false;
            btneliminar.Enabled = false;
            btnmodificar.Enabled = false;
            btnguardarmodificar.Visible = false;
            btncancelar.Visible = false;
            pb1.Enabled = false;
            txtstock.Enabled = false;

        }
        public void activarCampos()
        {
            txtcodigo.Enabled = true;
            txtcodigobar.Enabled = true;
            txtcodigofab.Enabled = true;
            txtdescrip.Enabled = true;
            txtcosto.Enabled = true;
            cmbunid.Enabled = true;
            txtprecioven.Enabled = true;
            txtproveedor.Enabled = true;
            txtclase.Enabled = true;
            txtmarca.Enabled = true;
            txtpais.Enabled = true;
            txtsubcalse.Enabled = true;
            txtmodelo.Enabled = true;
            cmbmoneda.Enabled = true;
            txtultil.Enabled = true;
            btnnuevo.Visible = true;
            btnguardar.Visible = true;
            pb1.Enabled = true;
            txtstock.Enabled = true;

        }
       

        

        private void btnregresar_Click(object sender, EventArgs e)
        {
            productoseleccionado?.Invoke(this, EventArgs.Empty);
            
            this.Close();
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            activarCampos();
            btnguardarmodificar.Visible = true;
            //  btnmodificar.Enabled = false;
            pb1.Enabled = true;
            txtnombrecodigo.Enabled = false;
            lbllistprod.Enabled = false;
            btncancelar.Visible = true;
            btneliminar.Enabled = false;
            btnregresar.Enabled = false;
            btnnuevo.Enabled = false;
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {
            if (validarCampos()) { 
                var produ = obtenerDatosForm();
            int lastid = datp.guardarproducto(produ);
                produ.idproducto = lastid;
            coleccionProducto.Add(produ);
                blockCampos();
                btnnuevo.Enabled = true;
                btnregresar.Enabled = true;
                txtnombrecodigo.Enabled = true;
                lbllistprod.Enabled = true;
            }

        }

        public Producto obtenerDatosForm()
        {
            Producto prod = new Producto();
            prod.idproducto = Convert.ToInt32(txtidprod.Text);
            prod.codigo = txtcodigo.Text;
            prod.codigo_bar = txtcodigobar.Text;
            prod.codigo_fab = txtcodigofab.Text;
            prod.articulo = txtdescrip.Text;
            prod.precio_con = Convert.ToDouble(txtprecioven.Text);
            prod.precio_cos = Convert.ToDouble(txtcosto.Text);
            prod.proveedor = txtproveedor.Text;
            prod.modelo = txtmodelo.Text;
            prod.marca = txtmarca.Text;
            prod.subclase = txtsubcalse.Text;
            prod.stock = Convert.ToInt64( txtstock.Text);
          //  prod.unimedida = cmbunid.SelectedItem.ToString();
            prod.unimedida = cmbunid.Text.ToString();
            // prod.moneda = cmbmoneda.SelectedItem.ToString();
            prod.moneda = cmbmoneda.Text.ToString();
            prod.pais = txtpais.Text;
            prod.clase = txtclase.Text;
            prod.utilidad = Convert.ToDecimal(modificarutilidad( txtultil.Text));


            if (foto.Equals("")) {
                prod.foto = "";
            }
            else
            {
                prod.foto = (foto);
            }

            return prod;
        }

        public void limpiarCampos()
        {
        
            txtcodigo.Text = "";
            txtcodigobar.Text = "";
            txtcodigofab.Text = "";
            txtdescrip.Text = "";
            txtcosto.Text = "";
          //  cmbunid.ResetText();
            txtprecioven.Text= "";
            txtproveedor.Text = "";
            txtclase.Text= "";
            txtmarca.Text = "";
            txtpais.Text="";
            txtsubcalse.Text = "";
            txtmodelo.Text = "";
           // cmbmoneda.ResetText();
            txtultil.Text = "";
            pb1.Image = null;
            txtnombrecodigo.Text = "";


            lbllistprod.DataSource = null;
            lbllistprod.DataSource = coleccionProducto;
            lbllistprod.DisplayMember = "articulo";
            lbllistprod.ValueMember = "idproducto";


            btnnuevo.Visible = true;
            txtstock.Text = "";    }
        public Boolean validarCampos()
        {
            Boolean valor = true;
            if (txtcodigo.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Codigo es obligatorio", "Validar", MessageBoxButtons.OK);
                txtcodigo.Focus();
                valor = false;
            }

               else if (txtcosto.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Costo es obligatorio", "Validar", MessageBoxButtons.OK);
                txtcosto.Focus();
                valor = false;
                return valor;
            }
            else if (txtprecioven.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Precio venta es obligatorio", "Validar", MessageBoxButtons.OK);
                txtprecioven.Focus();
                valor = false;
                return valor;
            }
            else if (txtdescrip.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Descripcion es obligatorio", "Validar", MessageBoxButtons.OK);
                txtdescrip.Focus();
                valor = false;
                return valor;
            }
            else if (txtstock.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Stock es obligatorio", "Validar", MessageBoxButtons.OK);
                txtstock.Focus();
                valor = false;
                return valor;
            }
            else if (cmbmoneda.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato moneda es obligatorio", "Validar", MessageBoxButtons.OK);
               cmbmoneda.Focus();
                valor = false;
                return valor;
            }
            else if (cmbunid.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Unidad es obligatorio", "Validar", MessageBoxButtons.OK);
                cmbunid.Focus();
                valor = false;
                return valor;
            }
            return valor;
        }

        private void btneliminar_Click_1(object sender, EventArgs e)
        {
            DialogResult resul = MessageBox.Show("Seguro que quieres eliminar el producto " + txtdescrip.Text + "?", "Eliminar Registro", MessageBoxButtons.YesNo);
            if (resul == DialogResult.Yes)
            {
                var prod = obtenerdatosid();
                coleccionProducto.Remove(prod);
                datp.eliminarProducto(prod.idproducto);
                limpiarCampos();
            }

        }

        public Producto obtenerdatosid()
        {
            if (lbllistprod.SelectedValue == null)
            {
                MessageBox.Show("Seleccione un Cliente, por favor");
                return null;
            }
            int idSeleccionado = Convert.ToInt32(lbllistprod.SelectedValue);
            var cliSel = coleccionProducto.SingleOrDefault(x => x.idproducto == idSeleccionado);
            if (cliSel == null)
            {
                return null;
            }
            return cliSel;
        
    }

        private void txtnombrecodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!txtnombrecodigo.Text.Equals(""))
                {
                    var c = txtnombrecodigo.Text.ToLower();
                    var filcliente = datp.filtrarProducto(c);
                    coleccionresult.Clear();
                    foreach (var item in filcliente)
                    {
                        coleccionresult.Add(item);
                    }
                    lbllistprod.DataSource = null;
                    lbllistprod.DataSource = coleccionresult;
                    lbllistprod.DisplayMember = "articulo";
                    lbllistprod.ValueMember = "idproducto";
                    var t = txtnombrecodigo.Text;
                    if (coleccionresult != null && coleccionresult.Count == 1)
                    {
                        obtenerdatos();
                    }
                }
                else if (txtnombrecodigo.Text.Equals(""))
                {
                    lbllistprod.DataSource = null;
                    lbllistprod.DataSource = coleccionProducto;
                    lbllistprod.DisplayMember = "articulo";
                    lbllistprod.ValueMember = "idproducto";
                    DialogResult resul = MessageBox.Show("Ingrese datos a buscar", "Mensaje", MessageBoxButtons.OK);
                }
            }
        }

        public void obtenerdatos()
        {
            var produ = obtenerdatosid();
            blockCampos();
            txtidprod.Text =Convert.ToString(produ.idproducto);
            txtcodigo.Text = produ.codigo;
            txtcodigobar.Text = produ.codigo_bar;
            txtcodigofab.Text = produ.codigo_fab;
            txtcosto.Text = Convert.ToString(produ.precio_cos);
            txtprecioven.Text = Convert.ToString(produ.precio_con);
            txtpais.Text = Convert.ToString(produ.pais);
            txtproveedor.Text = Convert.ToString(produ.proveedor);
            txtsubcalse.Text = Convert.ToString(produ.subclase);
            txtmarca.Text = Convert.ToString(produ.marca);
            txtmodelo.Text = Convert.ToString(produ.modelo);
            txtdescrip.Text = Convert.ToString(produ.articulo);
            txtultil.Text = Convert.ToString(produ.utilidad);
            cmbmoneda.Text = Convert.ToString(produ.moneda);
            cmbunid.Text = Convert.ToString(produ.unimedida);
            txtclase.Text = Convert.ToString(produ.clase);
            pb1.SizeMode = PictureBoxSizeMode.StretchImage;
            txtstock.Text = Convert.ToString(produ.stock);
           
           
            if (produ.foto == "") {
                
                pb1.Image = null;
            }
            else
            {
                foto = produ.foto;
                pb1.Load(foto);

            }
            foto = "";
         
            btnmodificar.Enabled = true;
            btneliminar.Enabled = true;
          
           


        }

        private void txtultil_KeyPress(object sender, KeyPressEventArgs e)
        {

            //if (e.KeyChar == 8)
            //{
            //    e.Handled = false;
            //    return;
            //}
            //bool IsDec = false;
            //int nroDec = 0;
            //for (int i = 0; i < txtultil.Text.Length; i++)
            //{
            //    if (txtultil.Text[i] == ',')
            //        IsDec = true;
            //    if (IsDec && nroDec++ > 3)
            //    {
            //        e.Handled = true;
            //        return;
            //    }
            //}
            //if (e.KeyChar >= 48 && e.KeyChar <= 57)
            //{
            //    e.Handled = false;
            //}
            //if (e.KeyChar == 44)
            //{
            //    e.Handled = (IsDec) ? true : false;
            //}
            //else
            //{
            //    e.Handled = true;
            //}
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar !=46 ))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }

           else if (e.KeyChar == 13)
            {
                porcentutil();
            }




        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            activarCampos();
            long ultimoid = (coleccionProducto.Count == 0) ? 0: coleccionProducto.Max(x => x.idproducto) + 1;
            txtidprod.Text = Convert.ToString(ultimoid);
            btnguardar.Visible = true;
            btncancelar.Visible = true;
            btnnuevo.Visible = false;
            limpiarCampos();
            btneliminar.Enabled = false;
            btnmodificar.Enabled = false;
            btnnuevo.Enabled = false;
            btnregresar.Enabled = false;
            txtnombrecodigo.Enabled = false;
            lbllistprod.Enabled = false;
            foto = "";
           


        }

        private void lbllistprod_DoubleClick(object sender, EventArgs e)
        {
           
        }

        private void btnguardarmodificar_Click(object sender, EventArgs e)
        {
            
            var produ = obtenerDatosForm();
            long id = produ.idproducto;
            // ventana
            int posicion = coleccionProducto.IndexOf(coleccionProducto.SingleOrDefault(x => x.idproducto == id));
            if (coleccionProducto[posicion].codigo != produ.codigo || coleccionProducto[posicion].codigo_bar != produ.codigo_bar ||
                coleccionProducto[posicion].codigo_fab != produ.codigo_fab || coleccionProducto[posicion].articulo != produ.articulo ||
                coleccionProducto[posicion].unimedida != produ.unimedida || coleccionProducto[posicion].precio_cos != produ.precio_cos
                || coleccionProducto[posicion].precio_con != produ.precio_con || coleccionProducto[posicion].marca !=produ.marca
                || coleccionProducto[posicion].modelo != produ.modelo || coleccionProducto[posicion].pais != produ.pais||
                coleccionProducto[posicion].proveedor !=produ.proveedor || coleccionProducto[posicion].subclase != produ.subclase
                || coleccionProducto[posicion].utilidad!=produ.utilidad|| coleccionProducto[posicion].moneda!=produ.moneda
                || coleccionProducto[posicion].clase!= produ.clase || coleccionProducto[posicion].foto != produ.foto || coleccionProducto[posicion].stock !=produ.stock)
            {
                coleccionProducto[posicion] = produ;
                produ.foto = produ.foto.Replace(@"\", "\\\\");
                // databse                        
                datp.ModificarProducto(produ);
                //limpiarCampos();
                blockCampos();
                btnguardarmodificar.Visible = false;
                btnnuevo.Enabled=true;
                btnregresar.Enabled = true;
                btnmodificar.Enabled = true;
                btneliminar.Enabled = true;
                txtnombrecodigo.Enabled = true;
                lbllistprod.Enabled = true;
                lbllistprod.DataSource = null;
                lbllistprod.DataSource = coleccionProducto;
                lbllistprod.DisplayMember = "articulo";
                lbllistprod.ValueMember = "idproducto";
            }
            else
            {
                DialogResult resul = MessageBox.Show("Debe modificar un dato", "Mensaje", MessageBoxButtons.OK);
                txtcodigo.Focus();
            }
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            //OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            //dialog.InitialDirectory = @"C:\";
            //dialog.Title = "Please select an image file to encrypt.";
            //dialog.ShowDialog();
            //pb1.SizeMode = PictureBoxSizeMode.StretchImage;
            //foto = modificarurl(dialog.FileName);
            //if(dialog.FileName == "")
            //{
            //    DialogResult resul = MessageBox.Show("ingrese una imagen", "Mensaje", MessageBoxButtons.OK);
            //}
            //else
            //{
            //    pb1.Load(dialog.FileName);
            //}
           

           
        }
        private string modificarurl(string url)
        {        
            return url.Replace("\\", "\\\\");

        }
        private string modificarurl2(string url)
        {
            return url.Replace(@"\","\\\\");

        }


        private String modificarutilidad(String url)
        {
            return url.Replace(",", ".");

        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            blockCampos();
            limpiarCampos();
            btnregresar.Enabled = true;
            btnnuevo.Enabled = true;
            lbllistprod.Enabled = true;
            txtnombrecodigo.Enabled = true;
        }

        public void porcentaje()
        {
                
            if (!txtcosto.Text.Equals("") )
            {
                decimal costo = Convert.ToDecimal(txtcosto.Text);
                decimal costoventa = Convert.ToDecimal(txtprecioven.Text);
                decimal utilidad;
                utilidad = (costoventa - costo);
                if (costo == 0)
                {
                    txtultil.Text = "0";
                }
                else
                {
                    utilidad = (utilidad / costo) * 100;
                    txtultil.Text = utilidad.ToString("N2");
                }
            }
            else
            {
                txtultil.Text ="0";
            }
            

        }

        private void txtprecioven_KeyPress(object sender, KeyPressEventArgs e)
        {

             if (e.KeyChar == 13)
            {
                porcentaje();
            }
           else if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar != 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
            
        }

        private void porcentutil()
        {
          
            if (!txtcosto.Text.Equals("") ) {
                int v = Convert.ToInt32(txtcosto.Text);
                if (v != 0)
                {
                    decimal costo = Convert.ToDecimal(txtcosto.Text);
                    decimal util = Convert.ToDecimal(txtultil.Text);
                    decimal utilidad;
                    utilidad = Convert.ToDecimal((util / 100) * costo);
                    utilidad = Convert.ToDecimal(utilidad + costo);
                    txtprecioven.Text = Convert.ToString(utilidad.ToString("N2"));
                }
            }

        }

        private void lbllistprod_MouseClick(object sender, MouseEventArgs e)
        {
            obtenerdatos();
        }

        private void txtprecioven_Leave(object sender, EventArgs e)
        {
            if (!txtcosto.Equals(""))
            {
                porcentaje();
            }
        }

        private void txtultil_Leave(object sender, EventArgs e)
        {
            if (!txtcosto.Equals(""))
            {
                porcentutil();
            }
            
        }

        private void txtcosto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back) && (e.KeyChar!= 46))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void txtstock_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                //MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }

        private void pb1_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            dialog.InitialDirectory = @"C:\";
            dialog.Title = "Please select an image file to encrypt.";
            dialog.ShowDialog();
            pb1.SizeMode = PictureBoxSizeMode.StretchImage;
            foto = modificarurl(dialog.FileName);
            if (dialog.FileName == "")
            {
                DialogResult resul = MessageBox.Show("ingrese una imagen", "Mensaje", MessageBoxButtons.OK);
            }
            else
            {
                pb1.Load(dialog.FileName);
            }
        }
    }
}
