﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
   public class Producto
    {
 
        public int idproducto { get; set; }
        public string codigo { get; set; }
        public string codigo_bar { get; set; }
        public string codigo_fab { get; set; }
        public string articulo { get; set; }
        //public Nullable<double> articulo_det { get; set; }
        public string clase { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        //public string serie { get; set; }
        //public string tipo { get; set; }
       // public Nullable<System.DateTime> fec_alta { get; set; }
        //public Nullable<long> stock_min { get; set; }
        public string unimedida { get; set; }
        //public string unimedidaf { get; set; }
        //public string unimedidam { get; set; }
        //public Nullable<long> fraccion { get; set; }
        //public Nullable<long> multiplo { get; set; }
        public Nullable<double> precio_con { get; set; }
        //public Nullable<double> precio_may1 { get; set; }
        //public Nullable<double> precio_may2 { get; set; }
        //public Nullable<double> precio_may3 { get; set; }
        //public Nullable<double> precio_fra { get; set; }
        //public Nullable<double> precio_cre { get; set; }
        //public Nullable<double> precio_pak { get; set; }
        public string moneda { get; set; }
        //public Nullable<double> peso { get; set; }
        //public Nullable<decimal> igv { get; set; }
        //public Nullable<decimal> descuento { get; set; }
        public Nullable<decimal> utilidad { get; set; }
        public string foto { get; set; }
        //public string descripcion { get; set; }
        //public string ubicacion { get; set; }
        public Nullable<double> precio_cos { get; set; }
        //public Nullable<double> precio_prm { get; set; }
        public Nullable<long> stock { get; set; }
        //public Nullable<sbyte> pesable { get; set; }
        //public Nullable<bool> lotes { get; set; }
        //public Nullable<bool> series { get; set; }
        //public Nullable<int> almacen { get; set; }
        //public Nullable<sbyte> exonerado { get; set; }
        //public string aplicacion { get; set; }
        public string pais { get; set; }
        public string proveedor { get; set; }
        //public string codfam { get; set; }
        public string subclase { get; set; }
        //public string tipo_existe { get; set; }
        //public string CX { get; set; }
        //public string CY { get; set; }
        //public string CZ { get; set; }
        //public string CA { get; set; }
        //public string CB { get; set; }
        //public string CD { get; set; }
        //public System.DateTime fecha_vto { get; set; }

    }
}
