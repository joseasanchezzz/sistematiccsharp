﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MClientes
{
    public partial class FRM_Reporte : Form
    {
        public FRM_Reporte()
        {
            InitializeComponent();
            reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
            reportViewer1.ZoomMode = ZoomMode.Percent;
            reportViewer1.ZoomPercent = 100;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
        }

        public void MostrarVistaPrevia(IList<Cliente> origenDatos, string tituloVentana)
        {
            this.Text = tituloVentana;
            //var lr = new LocalReport();
            //lr.DataSources.Add(new ReportDataSource("DsVenta", origenDatos));
            //lr.DataSources.Add(new ReportDataSource("DsVentaDetalle", origenDatos[0].Detalle));
            //lr.ReportPath = BasicLibrary.Utiles.CarpetaAplicacion + @"\Reportes\Venta.rdlc";
            //lr.PrintToPrinter();
            reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("dscliente", origenDatos));
            //reportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DsVentaDetalle", origenDatos[0].Detalle));
            reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\Report1.rdlc";
            //reportViewer1.LocalReport.PrintToPrinter();
            reportViewer1.RefreshReport();
        }
    }
}
