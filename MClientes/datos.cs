﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace MClientes
{
    public class DatosCliente
    {

        String cn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        private MySqlConnection cnn;
        public DatosCliente()
        {
            cnn = new MySqlConnection(cn);



        }

        //public DataTable todos()
        //{
        //    String sql = "select*from clientes";
        //    MySqlCommand cmd = new MySqlCommand(sql, cnn);

        //    //MySqlDataReader reader = new MySqlDataReader();

        //    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();

        //    da.Fill(dt);
        //    return dt;
        //}

        public List<Cliente> ObtenerClientes()
        {
            var lista = new List<Cliente>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from clientes";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unCliente = new Cliente();
                        unCliente.idcliente = Convert.ToInt32(reader["idcliente"]);
                        unCliente.cliente = Convert.ToString(reader["cliente"]);
                        unCliente.direccion = Convert.ToString(reader["direccion"]);
                        unCliente.ruc = Convert.ToString(reader["ruc"]);
                        unCliente.contacto = Convert.ToString(reader["contacto"]);
                        unCliente.email = Convert.ToString(reader["email"]);
                        unCliente.direccion2 = Convert.ToString(reader["direccion2"]);
                        unCliente.celular = Convert.ToString(reader["celular"]);
                        unCliente.telefono1 = Convert.ToString(reader["telefono1"]);
                        unCliente.telefono2 = Convert.ToString(reader["telefono2"]);
                        lista.Add(unCliente);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }

      

      
       public long guardarcliente(Cliente clie)
        {

            string query = "INSERT INTO clientes (cliente,contacto,ruc,direccion,telefono1,telefono2,celular,email,direccion2) values("
                + "'" + clie.cliente + "','" + clie.contacto+ "','" +clie.ruc + "','" + clie.direccion + "','" +clie.telefono1 + "','" + clie.telefono2 +
                "','" +clie.celular+ "','" + clie.email + "','" +clie.direccion2 + "')";

            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            int insertar;
            cnn.Open();
            insertar = cmd.ExecuteNonQuery();
            cmd.CommandText = "SELECT LAST_INSERT_ID()";
            long ultimoid = Convert.ToInt64(cmd.ExecuteScalar());
            cnn.Close();
            return ultimoid;
        }
        public void eliminarCliente(long id)
        {
           
                string conexion = "DELETE FROM clientes where idcliente=" + id;

                MySqlCommand cmd = new MySqlCommand(conexion, cnn);
                cmd.CommandType = CommandType.Text;
                cnn.Open();
                int eliminar = cmd.ExecuteNonQuery();
                cnn.Close();
            
        }

        public void ModificarCliente(Cliente clie)
        {

            string query = "UPDATE clientes SET cliente=" + "'" + clie.cliente + "' , contacto= '" + clie.contacto + "', ruc='" + clie.ruc + "', direccion= '"
                + clie.direccion + "', telefono1='" + clie.telefono1 + "', telefono2='" + clie.telefono2 + "', celular='" + clie.celular + "', email='" + clie.email +
                "', direccion2='" + clie.direccion2 + "'  where idcliente= " + clie.idcliente;
            MySqlCommand cmd = new MySqlCommand(query, cnn);
            cmd.CommandType = CommandType.Text;
            cnn.Open();
            int editar = Convert.ToInt16(cmd.ExecuteNonQuery());
            cnn.Close();
        }

        public List<Cliente> filtrarCliente(String cliente)
        {
            var lista = new List<Cliente>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;

                using (var cmd = new MySqlCommand())
                {
                                                                            
                    cmd.CommandText = "SELECT * FROM clientes WHERE cliente like  '%"+cliente +"%'" ;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unCliente = new Cliente();
                        unCliente.idcliente = Convert.ToInt32(reader["idcliente"]);
                        unCliente.cliente = Convert.ToString(reader["cliente"]);
                        unCliente.direccion = Convert.ToString(reader["direccion"]);
                        unCliente.ruc = Convert.ToString(reader["ruc"]);
                        unCliente.contacto = Convert.ToString(reader["contacto"]);
                        unCliente.email = Convert.ToString(reader["email"]);
                        unCliente.direccion2 = Convert.ToString(reader["direccion2"]);
                        unCliente.celular = Convert.ToString(reader["celular"]);
                        unCliente.telefono1 = Convert.ToString(reader["telefono1"]);
                        unCliente.telefono2 = Convert.ToString(reader["telefono2"]);
                        lista.Add(unCliente);
                    }
                    reader.Close();

                    cnx.Close();
                }
            }

            return lista;
        }


    }

}
