﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
     public class Detalle_com
    {
        public Nullable<long> idfactura { get; set; }
        public string idproducto { get; set; }
        public string codigo { get; set; }
        public string articulo { get; set; }
        public Nullable<decimal> cantidad { get; set; }
        public Nullable<decimal> precio { get; set; }
        public Nullable<decimal> valor_vta { get; set; }
        //public string moneda { get; set; }
        public string unimedida { get; set; }
        //public Nullable<long> fraccion { get; set; }
        //public Nullable<decimal> descuento { get; set; }
        //public string series { get; set; }
        //public decimal cantidad2 { get; set; }
    }
}
