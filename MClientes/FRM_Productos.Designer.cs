﻿namespace MClientes
{
    partial class FRM_Productos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelcodigo = new System.Windows.Forms.Label();
            this.txtcodigo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtcodigobar = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtcodigofab = new System.Windows.Forms.TextBox();
            this.btnnuevo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtdescrip = new System.Windows.Forms.TextBox();
            this.cmbunid = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtcosto = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtprecioven = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbmoneda = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtproveedor = new System.Windows.Forms.TextBox();
            this.txtpais = new System.Windows.Forms.TextBox();
            this.txtmodelo = new System.Windows.Forms.TextBox();
            this.txtmarca = new System.Windows.Forms.TextBox();
            this.txtsubcalse = new System.Windows.Forms.TextBox();
            this.txtclase = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnmodificar = new System.Windows.Forms.Button();
            this.btneliminar = new System.Windows.Forms.Button();
            this.btnregresar = new System.Windows.Forms.Button();
            this.lbllistprod = new System.Windows.Forms.ListBox();
            this.txtnombrecodigo = new System.Windows.Forms.TextBox();
            this.lbl1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtultil = new System.Windows.Forms.TextBox();
            this.btnguardar = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtidprod = new System.Windows.Forms.TextBox();
            this.btnguardarmodificar = new System.Windows.Forms.Button();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btncancelar = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtstock = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelcodigo
            // 
            this.labelcodigo.AutoSize = true;
            this.labelcodigo.Location = new System.Drawing.Point(28, 13);
            this.labelcodigo.Name = "labelcodigo";
            this.labelcodigo.Size = new System.Drawing.Size(43, 13);
            this.labelcodigo.TabIndex = 0;
            this.labelcodigo.Text = "Código:";
            // 
            // txtcodigo
            // 
            this.txtcodigo.Location = new System.Drawing.Point(83, 12);
            this.txtcodigo.Name = "txtcodigo";
            this.txtcodigo.Size = new System.Drawing.Size(100, 20);
            this.txtcodigo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Código de Barras:";
            // 
            // txtcodigobar
            // 
            this.txtcodigobar.Location = new System.Drawing.Point(307, 12);
            this.txtcodigobar.Name = "txtcodigobar";
            this.txtcodigobar.Size = new System.Drawing.Size(100, 20);
            this.txtcodigobar.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Código del Fabricante:";
            // 
            // txtcodigofab
            // 
            this.txtcodigofab.Location = new System.Drawing.Point(307, 41);
            this.txtcodigofab.Name = "txtcodigofab";
            this.txtcodigofab.Size = new System.Drawing.Size(100, 20);
            this.txtcodigofab.TabIndex = 5;
            // 
            // btnnuevo
            // 
            this.btnnuevo.Location = new System.Drawing.Point(10, 580);
            this.btnnuevo.Name = "btnnuevo";
            this.btnnuevo.Size = new System.Drawing.Size(75, 49);
            this.btnnuevo.TabIndex = 6;
            this.btnnuevo.Text = "Nuevo";
            this.btnnuevo.UseVisualStyleBackColor = true;
            this.btnnuevo.Click += new System.EventHandler(this.btnnuevo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Descripcion:";
            // 
            // txtdescrip
            // 
            this.txtdescrip.Location = new System.Drawing.Point(13, 77);
            this.txtdescrip.Multiline = true;
            this.txtdescrip.Name = "txtdescrip";
            this.txtdescrip.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtdescrip.Size = new System.Drawing.Size(394, 51);
            this.txtdescrip.TabIndex = 8;
            // 
            // cmbunid
            // 
            this.cmbunid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbunid.FormattingEnabled = true;
            this.cmbunid.Items.AddRange(new object[] {
            "UNIDAD"});
            this.cmbunid.Location = new System.Drawing.Point(13, 142);
            this.cmbunid.Name = "cmbunid";
            this.cmbunid.Size = new System.Drawing.Size(97, 21);
            this.cmbunid.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(144, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Costo:";
            // 
            // txtcosto
            // 
            this.txtcosto.Location = new System.Drawing.Point(186, 147);
            this.txtcosto.Name = "txtcosto";
            this.txtcosto.Size = new System.Drawing.Size(100, 20);
            this.txtcosto.TabIndex = 11;
            this.txtcosto.Text = "0.00";
            this.txtcosto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcosto_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(116, 169);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Precio venta:";
            // 
            // txtprecioven
            // 
            this.txtprecioven.Location = new System.Drawing.Point(186, 169);
            this.txtprecioven.Name = "txtprecioven";
            this.txtprecioven.Size = new System.Drawing.Size(100, 20);
            this.txtprecioven.TabIndex = 13;
            this.txtprecioven.Text = "0.00";
            this.txtprecioven.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtprecioven_KeyPress);
            this.txtprecioven.Leave += new System.EventHandler(this.txtprecioven_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(297, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Moneda:";
            // 
            // cmbmoneda
            // 
            this.cmbmoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbmoneda.FormattingEnabled = true;
            this.cmbmoneda.Items.AddRange(new object[] {
            "S/",
            "US$"});
            this.cmbmoneda.Location = new System.Drawing.Point(350, 171);
            this.cmbmoneda.Name = "cmbmoneda";
            this.cmbmoneda.Size = new System.Drawing.Size(55, 21);
            this.cmbmoneda.TabIndex = 15;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtproveedor);
            this.groupBox1.Controls.Add(this.txtpais);
            this.groupBox1.Controls.Add(this.txtmodelo);
            this.groupBox1.Controls.Add(this.txtmarca);
            this.groupBox1.Controls.Add(this.txtsubcalse);
            this.groupBox1.Controls.Add(this.txtclase);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(463, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(184, 164);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // txtproveedor
            // 
            this.txtproveedor.Location = new System.Drawing.Point(63, 125);
            this.txtproveedor.Name = "txtproveedor";
            this.txtproveedor.Size = new System.Drawing.Size(100, 20);
            this.txtproveedor.TabIndex = 11;
            // 
            // txtpais
            // 
            this.txtpais.Location = new System.Drawing.Point(62, 102);
            this.txtpais.Name = "txtpais";
            this.txtpais.Size = new System.Drawing.Size(100, 20);
            this.txtpais.TabIndex = 10;
            // 
            // txtmodelo
            // 
            this.txtmodelo.Location = new System.Drawing.Point(62, 78);
            this.txtmodelo.Name = "txtmodelo";
            this.txtmodelo.Size = new System.Drawing.Size(100, 20);
            this.txtmodelo.TabIndex = 9;
            // 
            // txtmarca
            // 
            this.txtmarca.Location = new System.Drawing.Point(62, 55);
            this.txtmarca.Name = "txtmarca";
            this.txtmarca.Size = new System.Drawing.Size(100, 20);
            this.txtmarca.TabIndex = 8;
            // 
            // txtsubcalse
            // 
            this.txtsubcalse.Location = new System.Drawing.Point(62, 32);
            this.txtsubcalse.Name = "txtsubcalse";
            this.txtsubcalse.Size = new System.Drawing.Size(100, 20);
            this.txtsubcalse.TabIndex = 7;
            // 
            // txtclase
            // 
            this.txtclase.Location = new System.Drawing.Point(62, 10);
            this.txtclase.Name = "txtclase";
            this.txtclase.Size = new System.Drawing.Size(100, 20);
            this.txtclase.TabIndex = 6;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Proveedor:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(30, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Pais:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Modelo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(20, 59);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Marca:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 36);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "SubClase:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 14);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Clase:";
            // 
            // btnmodificar
            // 
            this.btnmodificar.Location = new System.Drawing.Point(91, 580);
            this.btnmodificar.Name = "btnmodificar";
            this.btnmodificar.Size = new System.Drawing.Size(75, 49);
            this.btnmodificar.TabIndex = 17;
            this.btnmodificar.Text = "Modificar";
            this.btnmodificar.UseVisualStyleBackColor = true;
            this.btnmodificar.Click += new System.EventHandler(this.btnmodificar_Click);
            // 
            // btneliminar
            // 
            this.btneliminar.Location = new System.Drawing.Point(255, 580);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.Size = new System.Drawing.Size(75, 49);
            this.btneliminar.TabIndex = 18;
            this.btneliminar.Text = "Eliminar";
            this.btneliminar.UseVisualStyleBackColor = true;
            this.btneliminar.Click += new System.EventHandler(this.btneliminar_Click_1);
            // 
            // btnregresar
            // 
            this.btnregresar.Location = new System.Drawing.Point(600, 580);
            this.btnregresar.Name = "btnregresar";
            this.btnregresar.Size = new System.Drawing.Size(75, 49);
            this.btnregresar.TabIndex = 19;
            this.btnregresar.Text = "Regresar";
            this.btnregresar.UseVisualStyleBackColor = true;
            this.btnregresar.Click += new System.EventHandler(this.btnregresar_Click);
            // 
            // lbllistprod
            // 
            this.lbllistprod.FormattingEnabled = true;
            this.lbllistprod.Location = new System.Drawing.Point(13, 265);
            this.lbllistprod.Name = "lbllistprod";
            this.lbllistprod.Size = new System.Drawing.Size(176, 290);
            this.lbllistprod.TabIndex = 20;
            this.lbllistprod.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lbllistprod_MouseClick);
            this.lbllistprod.DoubleClick += new System.EventHandler(this.lbllistprod_DoubleClick);
            // 
            // txtnombrecodigo
            // 
            this.txtnombrecodigo.Location = new System.Drawing.Point(14, 239);
            this.txtnombrecodigo.Name = "txtnombrecodigo";
            this.txtnombrecodigo.Size = new System.Drawing.Size(175, 20);
            this.txtnombrecodigo.TabIndex = 21;
            this.txtnombrecodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnombrecodigo_KeyPress);
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Location = new System.Drawing.Point(18, 223);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(146, 13);
            this.lbl1.TabIndex = 22;
            this.lbl1.Text = "Buscar por Codigo o Nombre:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(297, 154);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 25;
            this.label14.Text = "Util %";
            // 
            // txtultil
            // 
            this.txtultil.Location = new System.Drawing.Point(350, 150);
            this.txtultil.MaxLength = 6;
            this.txtultil.Name = "txtultil";
            this.txtultil.Size = new System.Drawing.Size(55, 20);
            this.txtultil.TabIndex = 26;
            this.txtultil.Text = "0.00";
            this.txtultil.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtultil_KeyPress);
            this.txtultil.Leave += new System.EventHandler(this.txtultil_Leave);
            // 
            // btnguardar
            // 
            this.btnguardar.Location = new System.Drawing.Point(91, 580);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(75, 49);
            this.btnguardar.TabIndex = 27;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.UseVisualStyleBackColor = true;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(85, 35);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(18, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "ID";
            // 
            // txtidprod
            // 
            this.txtidprod.Enabled = false;
            this.txtidprod.Location = new System.Drawing.Point(109, 32);
            this.txtidprod.Name = "txtidprod";
            this.txtidprod.Size = new System.Drawing.Size(44, 20);
            this.txtidprod.TabIndex = 29;
            // 
            // btnguardarmodificar
            // 
            this.btnguardarmodificar.Location = new System.Drawing.Point(91, 580);
            this.btnguardarmodificar.Name = "btnguardarmodificar";
            this.btnguardarmodificar.Size = new System.Drawing.Size(75, 49);
            this.btnguardarmodificar.TabIndex = 30;
            this.btnguardarmodificar.Text = "Guardar";
            this.btnguardarmodificar.UseVisualStyleBackColor = true;
            this.btnguardarmodificar.Click += new System.EventHandler(this.btnguardarmodificar_Click);
            // 
            // pb1
            // 
            this.pb1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pb1.Location = new System.Drawing.Point(307, 286);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(239, 202);
            this.pb1.TabIndex = 31;
            this.pb1.TabStop = false;
            this.pb1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            this.pb1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pb1_MouseClick);
            // 
            // btncancelar
            // 
            this.btncancelar.Location = new System.Drawing.Point(172, 580);
            this.btncancelar.Name = "btncancelar";
            this.btncancelar.Size = new System.Drawing.Size(75, 49);
            this.btncancelar.TabIndex = 32;
            this.btncancelar.Text = "Cancelar";
            this.btncancelar.UseVisualStyleBackColor = true;
            this.btncancelar.Click += new System.EventHandler(this.btncancelar_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(468, 189);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Stock:";
            // 
            // txtstock
            // 
            this.txtstock.Location = new System.Drawing.Point(525, 186);
            this.txtstock.Name = "txtstock";
            this.txtstock.Size = new System.Drawing.Size(100, 20);
            this.txtstock.TabIndex = 34;
            this.txtstock.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtstock_KeyPress);
            // 
            // FRM_Productos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 641);
            this.Controls.Add(this.txtstock);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btncancelar);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.btnguardarmodificar);
            this.Controls.Add(this.txtidprod);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.btnguardar);
            this.Controls.Add(this.txtultil);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.txtnombrecodigo);
            this.Controls.Add(this.lbllistprod);
            this.Controls.Add(this.btnregresar);
            this.Controls.Add(this.btneliminar);
            this.Controls.Add(this.btnmodificar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbmoneda);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtprecioven);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtcosto);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbunid);
            this.Controls.Add(this.txtdescrip);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnnuevo);
            this.Controls.Add(this.txtcodigofab);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcodigobar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcodigo);
            this.Controls.Add(this.labelcodigo);
            this.Name = "FRM_Productos";
            this.Text = "Productos";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelcodigo;
        private System.Windows.Forms.TextBox txtcodigo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcodigobar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtcodigofab;
        private System.Windows.Forms.Button btnnuevo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtdescrip;
        private System.Windows.Forms.ComboBox cmbunid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtcosto;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtprecioven;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbmoneda;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtproveedor;
        private System.Windows.Forms.TextBox txtpais;
        private System.Windows.Forms.TextBox txtmodelo;
        private System.Windows.Forms.TextBox txtmarca;
        private System.Windows.Forms.TextBox txtsubcalse;
        private System.Windows.Forms.TextBox txtclase;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnmodificar;
        private System.Windows.Forms.Button btneliminar;
        private System.Windows.Forms.Button btnregresar;
        private System.Windows.Forms.ListBox lbllistprod;
        private System.Windows.Forms.TextBox txtnombrecodigo;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtultil;
        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtidprod;
        private System.Windows.Forms.Button btnguardarmodificar;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btncancelar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtstock;
    }
}