﻿using BasicLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MClientes
{
    public partial class FRM_Menu : Form
    {
        public FRM_Menu()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_Productos frm = new FRM_Productos();
            frm.Show();
        }

        private void comprasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VentanaDetalles frm5 = new VentanaDetalles();
            frm5.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
           FRM_Clientes frm1 = new FRM_Clientes();
            frm1.Show();
        }

        private void proveedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRM_Proveedor pro = new FRM_Proveedor();
            pro.Show();
        }
    }
}
