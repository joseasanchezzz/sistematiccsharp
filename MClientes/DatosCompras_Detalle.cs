﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.Windows.Forms;

namespace MClientes
{
    public class DatosCompras_Detalle
    {
        String cn = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        private MySqlConnection cnn;

        public DatosCompras_Detalle() 
        {
            cnn = new MySqlConnection(cn);
        }

        public List<Producto> ObtenerProducto()
        {
            var lista = new List<Producto>();

            using (var cnx = new MySqlConnection())
            {
                cnx.ConnectionString = cn;
                using (var cmd = new MySqlCommand())
                {
                    cmd.CommandText = "select * from productos";
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = cnx;
                    cnx.Open();
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        var unProducto = new Producto();
                        unProducto.idproducto = Convert.ToInt32(reader["idproducto"]);
                        unProducto.codigo_bar = Convert.ToString(reader["codigo_bar"]);
                        unProducto.codigo = Convert.ToString(reader["codigo"]);
                        unProducto.codigo_fab = Convert.ToString(reader["codigo_fab"]);
                        unProducto.articulo = Convert.ToString(reader["articulo"]);
                        unProducto.precio_con = Convert.ToDouble(reader["precio_con"]);
                        unProducto.precio_cos = Convert.ToDouble(reader["precio_cos"]);
                        unProducto.unimedida = Convert.ToString(reader["unimedida"]);
                        unProducto.proveedor = Convert.ToString(reader["proveedor"]);
                        unProducto.clase = Convert.ToString(reader["clase"]);
                        unProducto.pais = Convert.ToString(reader["pais"]);
                        unProducto.subclase = Convert.ToString(reader["subclase"]);
                        unProducto.modelo = Convert.ToString(reader["modelo"]);
                        unProducto.moneda = Convert.ToString(reader["moneda"]);
                        unProducto.utilidad = Convert.ToDecimal(reader["utilidad"]);
                        unProducto.marca = Convert.ToString(reader["marca"]);
                        unProducto.foto = Convert.ToString(reader["foto"]);
                        unProducto.stock = Convert.ToInt64(reader["stock"]);
                        lista.Add(unProducto);
                    }
                    reader.Close();
                    cnx.Close();
                }
            }
            return lista;
        }

        //       SELECT student.firstname,
        //      student.lastname,
        //      exam.name,
        //      exam.date,
        //      grade.grade
        // FROM grade
        //INNER JOIN student ON student.studentId = grade.fk_studentId
        //INNER JOIN exam ON exam.examId = grade.fk_examId
        //ORDER BY exam.date



    }
}
