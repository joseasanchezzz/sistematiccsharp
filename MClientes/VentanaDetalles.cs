﻿using BasicLibrary;
//using DataAccess.Nucleo.Dominio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
//using VentasUI.Reportes;
//using VentasUI.Reportes.Origenes;
//using VentasUI.Ventanas.Auxiliares;

namespace MClientes
{
    public partial class VentanaDetalles : Form
    {
        private AuxiliarDatos auxiliar;
        private BindingList<Proveedores> coleccionProveedores;
        FRM_Proveedor frmprove = new FRM_Proveedor();
        FRM_Productos frmproducto = new FRM_Productos();

        private decimal SubTotal
        {
            get { return txtSubtotal.ValorDecimal; }

            set { txtSubtotal.ValorDecimal = value; }
        }
        private decimal Igv
        {
            get { return txtIgv.ValorDecimal; }

            set { txtIgv.ValorDecimal = value; }
        }
        private decimal Total
        {
            get { return txtTotal.ValorDecimal; }

            set { txtTotal.ValorDecimal = value; }
        }

     
        private Producto productoActual;
        private bool unoPorDefecto;
        private int cantidad;

        public VentanaDetalles()
        {
            InitializeComponent();
            InicializarComponentes();
            InicializarControles();
            inicombo();
            frmprove.proveedorseleccionado += Frmpro_proveedorseleccionado;
            frmproducto.productoseleccionado += Frmproducto_productoseleccionado;


        }

        private void Frmproducto_productoseleccionado(object sender, EventArgs e)
        {
            ObtenerProductos();
        }

        private void Frmpro_proveedorseleccionado(object sender, EventArgs e)
        {
            inicombo();
        }

        private void InicializarComponentes()
        {
            auxiliar = AuxiliarDatos.InstanciaUnica;
            //auxiliar.VentaActualModificada += Auxiliar_VentaActualModificada;
            //SesionActual = Sesion.SoloLectura;
            ObtenerProductos();
            //imprenta = new Imprenta();
            unoPorDefecto = true;
            cantidad = 1;
        }

        public void inicombo()
        {
            coleccionProveedores = new  BindingList<Proveedores>(auxiliar.obtenerProveedorBd());
            cmbprove.DataSource = coleccionProveedores;
            cmbprove.DisplayMember = "proveedor";
            cmbprove.ValueMember = "idproveedor";
            cmbprove.SelectedIndex = -1;
             }

        private void InicializarControles()
        {
            // TextBox de códigos
            txtPosibleCodigo.AutoCompleteSource = AutoCompleteSource.CustomSource;
            txtPosibleCodigo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            txtPosibleCodigo.CharacterCasing = CharacterCasing.Upper;
            // Grilla
            dgvDetalles.AutoGenerateColumns = false;
            dgvDetalles.DataSource = auxiliar.ColeccionDetalles;
            // Uno por defecto
            chkUno.Checked = true;
            // Controles numéricos
            txtCantidad.ValorInt = cantidad;
        }

        private void ObtenerProductos()
        {
            auxiliar.ObtenerProductos();
            var listaCodigos = new AutoCompleteStringCollection();
            listaCodigos.AddRange(auxiliar.ColeccionCodigos.ToArray());
            txtPosibleCodigo.AutoCompleteCustomSource = null;
            txtPosibleCodigo.AutoCompleteCustomSource = listaCodigos;
        }

        public Compra obtenerDatosForm()
        {
            Compra comp = new Compra();
            comp.idproveedor = Convert.ToInt64(cmbprove.SelectedValue);
            comp.documento = txtdocumento.Text;
            comp.subtotal = Convert.ToDouble( txtSubtotal.Text);
            comp.igv = Convert.ToDouble(txtIgv.Text);
            comp.total =Convert.ToDouble( txtTotal.Text);
            comp.fecha =Convert.ToDateTime( datapic.Value);
            comp.tipodoc = "FV";
            return comp;
        }

        private void Auxiliar_VentaActualModificada(object sender, EventArgs e)
        {
            //txtPosibleCodigo.Clear();

            //// Si la venta actual es null, se limpian los controles,
            //// de lo contrario se cargan los datos en ellos
            //bool ventaExiste = (auxiliar.VentaActual != null);

            //labCaptionFecha.Visible =
            //labCaptionHora.Visible  =
            //labFecha.Visible        =
            //labHora.Visible         = (ventaExiste);

            //labDocumento.Text = (!ventaExiste) ? "" : auxiliar.VentaActual.documento;
            //labFecha.Text     = (!ventaExiste) ? "" : $"{auxiliar.VentaActual.fecha:dd/MM/yyyy}";
            //labHora.Text      = (!ventaExiste) ? "" : auxiliar.VentaActual.hora;

            //labCajero.Text    = (ventaExiste) ? auxiliar.VentaActual.usuario
            //                                  : auxiliar.UsuarioActual.login;

            //SubTotal          = (!ventaExiste) ? 0M : (decimal)auxiliar.VentaActual.subtotal;
            //Igv               = (!ventaExiste) ? 0M : (decimal)auxiliar.VentaActual.igv;
            //Total             = (!ventaExiste) ? 0M : (decimal)auxiliar.VentaActual.total;
        }

        private void HabilitarTabEnControles(bool habilitar)
        {
            //groupBox1.TabStop =
            //txtPosibleCodigo.TabStop =
            //txtCantidad.TabStop =
            //chkUno.TabStop =
            //btnQuitar.TabStop =
            //dgvDetalles.TabStop =
            //txtSubtotal.TabStop =
            //txtIgv.TabStop =
            //txtTotal.TabStop = (habilitar);
        }

        //public void SesionSoloLectura()
        //{
        //    colIdProducto.Visible = false;

        //    SesionActual = Sesion.SoloLectura;

        //    this.Text = "Venta";
        //    string doc = auxiliar.TiposDoc.SingleOrDefault(x => x.Key == auxiliar.CodigoTipoDoc).Value;
        //    groupBox1.Text = doc;

        //    btnCancelar.Select();
        //}

        //public void SesionInsertando(string nuevoDocumento)
        //{
        //    colIdProducto.Visible = false;

        //    SesionActual = Sesion.Insertando;

        //    this.Text = "Registrando una venta nueva...";
        //    string doc = auxiliar.TiposDoc.SingleOrDefault(x => x.Key == auxiliar.CodigoTipoDoc).Value;
        //    groupBox1.Text = doc;

        //    labDocumento.Text = nuevoDocumento;
            
        //    txtPosibleCodigo.Select();
        //}

        //public void SesionModificando()
        //{
        //    colIdProducto.Visible = false;

        //    SesionActual = Sesion.Modificando;

        //    this.Text = "Modificando una venta...";
        //    groupBox1.Text = auxiliar.TiposDoc.SingleOrDefault(x => x.Value == auxiliar.CodigoTipoDoc).Value;

        //    txtPosibleCodigo.Select();
        //}



        private void txtPosibleCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                string posibleCodigo = txtPosibleCodigo.Text;
                if (String.IsNullOrWhiteSpace(posibleCodigo))
                {
                    Mensaje.Advertencia("Es necesario el código del producto.");
                    txtPosibleCodigo.Select();
                    return;
                }

                // Busca el código y según lo encuentra lo establece a productoActual
                BuscarProductoPorCodigo(posibleCodigo);
                if (productoActual == null)
                {
                    Mensaje.Advertencia($"No se encontró el código {posibleCodigo}.");
                    txtPosibleCodigo.Select();
                    return;
                }
                if (unoPorDefecto)
                {
                    AgregarDetalle();
                }
                else
                {
                    txtCantidad.Select();
                }
            }
        }

        private void BuscarProductoPorCodigo(string codigo)
        {
            productoActual = auxiliar.ColeccionProductos.FirstOrDefault(x => x.codigo == codigo);
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                e.Handled = true;

                string posibleCodigo = txtPosibleCodigo.Text;

                if (String.IsNullOrWhiteSpace(posibleCodigo))
                {
                    Mensaje.Advertencia("Es necesario el código del producto.");
                    txtPosibleCodigo.Select();
                    return;
                }

                if (txtCantidad.ValorInt < 1)
                {
                    Mensaje.Advertencia("Especifique la cantidad.");
                    txtCantidad.Select();
                    return;
                }
                AgregarDetalle();
            }
        }

        // Detalles
        private void AgregarDetalle()
        {
            string idProd = Convert.ToString(productoActual.idproducto);

            var unProd = auxiliar.ColeccionDetalles.SingleOrDefault(x => x.idproducto == idProd);

            if (unProd != null)
            {
                int posicion = auxiliar.ColeccionDetalles.IndexOf(unProd);

                var det = auxiliar.ColeccionDetalles[posicion];

                //  si uno por defecto,
                var c = (unoPorDefecto) ? 1 : txtCantidad.ValorInt;

                det.cantidad += c;
                det.valor_vta = det.precio * det.cantidad;

                auxiliar.ColeccionDetalles[posicion] = det;
            }
            else
            {
                var detalle = new Detalle_com();

                cantidad = txtCantidad.ValorInt;
                // Datos de los controles
                detalle.idproducto = Convert.ToString(productoActual.idproducto);
                detalle.codigo = productoActual.codigo;
                detalle.articulo = productoActual.articulo;
                detalle.cantidad = cantidad;
                detalle.precio = (decimal?)productoActual.precio_con;
                detalle.valor_vta = (detalle.cantidad * detalle.precio);
                // Otros datos de la tabla
                //detalle.articulo_det  = null;
                //detalle.descuento     = 0;
                //detalle.IGV           = 18;
                //detalle.moneda        = null;
                detalle.unimedida = productoActual.unimedida;
                //detalle.fraccion      = null;
                //detalle.series        = null;
                //detalle.almacen       = 1;
                //detalle.descuento_tex = "";
                //detalle.cantidad2     = 0;
                InsertarDetalleEnColeccion(detalle);
                CalcularTotales();
                // Despues de agregar el detalle, se reenfoca txtPosibleCodigo
                txtPosibleCodigo.Clear();
                txtPosibleCodigo.Select();
            }

            CalcularTotales();

            // Despues de agregar el detalle, se reenfoca txtPosibleCodigo
            txtPosibleCodigo.Clear();
            txtPosibleCodigo.Select();
        }
        

        private void InsertarDetalleEnColeccion(Detalle_com detalleNuevo)
        {
            // Va insertando al inicio de la lista
            auxiliar.ColeccionDetalles.Insert(0, detalleNuevo);

            // Se selecciona la primera fila
            dgvDetalles.Rows[0].Selected = true;

            dgvDetalles.CurrentCell = dgvDetalles.Rows[0].Cells["colCodigo"];

            if (dgvDetalles.CurrentRow != null)
            {
                dgvDetalles.CurrentRow.Selected = true;
            }
        }

        private void QuitarDetalle()
        {
            if (dgvDetalles.SelectedRows.Count < 1)
            {
                Mensaje.Advertencia("Selecciona el detalle que quieres quitar.");

                return;
            }

            var detalleSeleccionado = (dgvDetalles.BindingContext[auxiliar.ColeccionDetalles].Current) as Detalle_com;
            auxiliar.ColeccionDetalles.Remove(detalleSeleccionado);

            CalcularTotales();
        }

        private void CalcularTotales()
        {
            const decimal valorIgv = 0.18M;

            decimal? suma = auxiliar.ColeccionDetalles.Sum(x => x.valor_vta);

            SubTotal = (decimal)((suma.HasValue) ? suma : 0M);
            Igv      = SubTotal * valorIgv;
            Total    = SubTotal + Igv;
        }

        // Guardar
        private void GuardarVenta()
        {
            if (dgvDetalles.Rows.Count < 1)
            {
                Mensaje.Advertencia("Para guardar es necesario agregar al menos un detalle.");

                txtPosibleCodigo.Select();

                return;
            }

            //switch (SesionActual)
            //{
            //    case Sesion.Insertando:
            //        InsertarVenta();
            //        break;

            //    case Sesion.Modificando:
            //        ModificarVenta();
            //        break;
            //}
        }

        private void InsertarVenta()
        {
            //// Obtiene los datos de la venta desde los controles
            //// y se envía a AuxiliarDatos para insertar
            //var ventaNueva = ObtenerVentaDesdeControles();

            //auxiliar.InsertarNuevaVenta(ventaNueva);
            
            //// Avisa a la ventana cliente que se guardó una nueva venta
            //VentaGuardada?.Invoke(this, new VentaGuardadaEventArgs(true, ventaNueva.idfactura));

            //SesionActual = Sesion.SoloLectura;

            //// Después de guardar, muestra el código del documento y la fecha y hora
            //labDocumento.Text = ventaNueva.documento;

            //labCaptionFecha.Visible = true;
            //labFecha.Text = $"{ventaNueva.fecha:dd/MM/yyyy}";
            //labFecha.Visible = true;

            //labCaptionHora.Visible = true;
            //labHora.Text = $"{ventaNueva.fecha:HH:mm:ss}";
            //labHora.Visible = true;

            //// Pregunta si se desea imprimir
            //if (Mensaje.ConsultaAceptada("Se guardaron correctamente los datos.\r\n¿Deseas imprimirlos?"))
            //{
            //    VistaPrevia();
            //}

            //// Pregunta si desea hacer otro documento
            //if (Mensaje.ConsultaAceptada("¿Deseas hacer uno nuevo?"))
            //{
            //    auxiliar.VentaActual = null;
            //    auxiliar.ColeccionDetalles.Clear();

            //    string documentosiguiente = auxiliar.ObtenerDocumentoSiguiente();

            //    SesionInsertando(documentosiguiente);
            //}
            //else
            //{
            //    this.Close();
            //}
        }

        private void ModificarVenta()
        {
            //// Obtiene los datos de la venta desde los controles
            //// y se envía a AuxiliarDatos para insertar
            //var ventaModificada = ObtenerVentaDesdeControles();

            //auxiliar.ModificarVenta(ventaModificada);

            //// Avisa a la ventana cliente que se guardaron los cambios una venta existente
            //VentaGuardada?.Invoke(this, new VentaGuardadaEventArgs(false, ventaModificada.idfactura));
        }

        //private ventas ObtenerVentaDesdeControles()
        //{
        //    var unaVenta = new ventas();

        //    bool insertando = (SesionActual == Sesion.Insertando);

        //    // Datos de los controles
        //    unaVenta.tipodoc   = (insertando) ? auxiliar.CodigoTipoDoc : auxiliar.VentaActual.tipodoc;
        //    unaVenta.documento = (insertando) ? labDocumento.Text      : auxiliar.VentaActual.documento;
        //    unaVenta.fecha     = (insertando) ? DateTime.Now           : auxiliar.VentaActual.fecha;
        //    unaVenta.subtotal  = (double?)SubTotal;
        //    unaVenta.igv       = (double?)Igv;
        //    unaVenta.total     = (double?)Total;
        //    unaVenta.usuario   = (insertando) ? auxiliar.UsuarioActual.login      : auxiliar.VentaActual.usuario;
        //    unaVenta.hora      = (insertando) ? DateTime.Now.ToString("HH:mm:ss") : auxiliar.VentaActual.hora;

        //    // Otros datos de la tabla
        //    unaVenta.proforma      = "";
        //    unaVenta.guia          = "";
        //    unaVenta.ocompra       = "";
        //    unaVenta.opedido       = "";
        //    unaVenta.idcliente     = 1;
        //    unaVenta.fecha_vto     = (insertando) ? DateTime.Now : auxiliar.VentaActual.fecha_vto;
        //    unaVenta.idvendedor    = 0;
        //    unaVenta.bruto         = (double?)Total;
        //    unaVenta.descuento     = 0;
        //    unaVenta.observacion   = "";
        //    unaVenta.estado        = "a";
        //    unaVenta.estado_can    = "C";
        //    unaVenta.inicial       = (double?)Total;
        //    unaVenta.saldo         = 0;
        //    unaVenta.moneda        = "S/";
        //    unaVenta.cambio        = 0;
        //    unaVenta.condiciones   = "CONTADO";
        //    unaVenta.num_kardex    = null;
        //    // Id autogenerado de la tabla caja
        //    unaVenta.idcaja        = (insertando) ? null: auxiliar.VentaActual.idcaja;
        //    unaVenta.bol_cliente   = "";
        //    unaVenta.bol_direccion = "";
        //    unaVenta.bol_dni       = "";
        //    unaVenta.anulada       = false;
        //    unaVenta.impresa       = null;
        //    unaVenta.descargada    = 1;
        //    unaVenta.local         = 1;
        //    unaVenta.paga_con      = "";
        //    unaVenta.flete         = null;
        //    unaVenta.empresa       = null;
        //    unaVenta.isc           = 0;
        //    unaVenta.tra_par       = "";
        //    unaVenta.tra_lle       = "";
        //    unaVenta.tra_cho       = "";
        //    unaVenta.tra_ruc       = "";
        //    unaVenta.tra_pla       = "";
        //    unaVenta.tra_mtc       = "";
        //    unaVenta.peso          = 0;
        //    unaVenta.exonerado     = 0.00M;
        //    unaVenta.efectivo      = 0;
        //    unaVenta.caja          = 1;
        //    unaVenta.turno         = false;
        //    unaVenta.tarjeta       = 0;
        //    unaVenta.ref_tarjeta   = "";
        //    unaVenta.percepcion    = 0;
        //    unaVenta.detraccion    = 0;
        //    unaVenta.idnotav       = 0;
        //    unaVenta.fecha_can     = new DateTime(1, 1, 1);

        //    return unaVenta;
        //}

        






        #region Printing

        private void VistaPrevia()
        {
            //imprenta.VentaVistaPrevia(ObtenerOrigenDatos());
        }

        //private List<DtoVenta> ObtenerOrigenDatos()
        //{
        //    var listaVentas = new List<DtoVenta>(1);

        //    var unaVenta = new DtoVenta();

        //    unaVenta.TipoDoc = auxiliar.CodigoTipoDoc;
        //    unaVenta.Documento = labDocumento.Text;
        //    unaVenta.FechaHora = $"{labFecha.Text} {labHora.Text}";
        //    unaVenta.Cajero = labCajero.Text;
        //    unaVenta.Subtotal = (double)SubTotal;
        //    unaVenta.Igv = (double)Igv;
        //    unaVenta.Total = (double)Total;
        //    unaVenta.Detalle = new List<DtoDetalleVenta>(auxiliar.ColeccionDetalles.Count);

        //    foreach (var detalle in auxiliar.ColeccionDetalles)
        //    {
        //        var detalleNuevo = new DtoDetalleVenta();

        //        detalleNuevo.Codigo = detalle.codigo;
        //        detalleNuevo.Articulo = detalle.articulo;
        //        detalleNuevo.Cantidad = detalle.cantidad;
        //        detalleNuevo.Precio = detalle.precio;
        //        detalleNuevo.Importe = detalle.valor_vta;

        //        unaVenta.Detalle.Add(detalleNuevo);
        //    }

        //    listaVentas.Add(unaVenta);

        //    return listaVentas;
        //}

        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    switch (keyData)
        //    {
        //        case Keys.F4:
        //            if (SesionActual != Sesion.Insertando)
        //            {
        //                VistaPrevia();
        //            }
        //            break;

        //        case Keys.F9:
        //            if (SesionActual != Sesion.SoloLectura)
        //            {
        //                GuardarVenta();
        //            }
        //            break;
        //    }

        //    return base.ProcessCmdKey(ref msg, keyData);
        //}

        #endregion

        private void dgvDetalles_SelectionChanged(object sender, EventArgs e)
        {


            //var estiloCelda = new DataGridViewCellStyle();
            //estiloCelda.Padding = new Padding(Column1.Width + 1, 0, 0, 0);

            //dgvDetalles.Rows[0].Cells[6].Style = estiloCelda;
            /*System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
            dataGridViewCellStyle2->Padding = System::Windows::Forms::Padding(25, 0, 0, 0);

            dgv1->Rows[0]->Cells[0]->Style = dataGridViewCellStyle2;*/
        }


        private void chkUno_CheckedChanged(object sender, EventArgs e)
        {
            unoPorDefecto = chkUno.Checked;

            txtCantidad.ReadOnly = unoPorDefecto;
            txtCantidad.ValorInt = 1;

            if (chkUno.Checked == false)
            {
                txtCantidad.SelectAll();
                txtCantidad.Select();
            }
        }

        private void dgvDetalles_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case (Keys.Enter):
                    e.SuppressKeyPress = true;
                    break;

                case (Keys.Delete):
                    QuitarDetalle();
                    break;
            }
        }

        private void txtCantidad_Leave(object sender, EventArgs e)
        {
            if (txtCantidad.ValorInt < 1)
            {
                txtCantidad.ValorInt = 1;
            }
        }

        // Botones
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //if (auxiliar.ColeccionDetalles.Count < 1)
            //{
            //    Mensaje.Advertencia("Es necesario al menos un detalle para registrar la venta.");

            //    txtPosibleCodigo.Select();

            //    return;
            //}

            //GuardarVenta();
            int ultimoid = auxiliar.guardarCabecera(obtenerDatosForm());
            Console.WriteLine(ultimoid);
            //auxiliar.ColeccionDetalles.
            foreach (var item in auxiliar.ColeccionDetalles)
            {
                item.idfactura = ultimoid;
                auxiliar.guardarDetalle_com(item);
            }
            limpiarCampos();

           
        
        }

        public void limpiarCampos()
        {
            txtdocumento.Text = "";
            datapic.Text = "";
            cmbprove.SelectedIndex = -1;
            txtCantidad.Text = "";
            txtSubtotal.Text = "";
            txtIgv.Text = "";
            txtTotal.Text = "";
            auxiliar.ColeccionDetalles.Clear();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            VistaPrevia();
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            QuitarDetalle();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
            Dispose();
        }

        private void btnmasprove_Click(object sender, EventArgs e)
        {

            frmprove.ShowDialog(this);

        }

        private void btnmasproductos_Click(object sender, EventArgs e)
        {
           
            frmproducto.ShowDialog(this);
        }
    }
}