﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
    public class Proveedores
    {
       
            public long idproveedor { get; set; }
            public string codigo { get; set; }
            public string proveedor { get; set; }
            public string contacto { get; set; }
            public string ruc { get; set; }
            public string direccion { get; set; }
            public string telefono1 { get; set; }
            public string telefono2 { get; set; }
            public string celular { get; set; }
            public string email { get; set; }
            public string productos { get; set; }
            public string giro { get; set; }
            public string ciudad { get; set; }
            public string ruta { get; set; }

        
        
    }
}
