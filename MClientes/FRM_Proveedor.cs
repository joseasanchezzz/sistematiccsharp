﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MClientes
{
    public partial class FRM_Proveedor : Form
    {

        private BindingList<Proveedores> coleccionProveedores;
        private BindingList<Proveedores> coleccionresult;
        datosProveedor datpro = new datosProveedor();
        public event EventHandler<EventArgs> proveedorseleccionado;

        public FRM_Proveedor()
        {
            InitializeComponent();
            inicampos();
            coleccionProveedores = new BindingList<Proveedores>(datpro.ObtenerProveedor());
            coleccionresult = new BindingList<Proveedores>();
            lbllistanombre.DataSource = coleccionProveedores;
            //lbllistanombre.DataSource = tablaClientes;
            lbllistanombre.DisplayMember = "proveedor";
            lbllistanombre.ValueMember = "idproveedor";
            this.FormClosing += FRM_Proveedor_FormClosing;
        }

        private void FRM_Proveedor_FormClosing(object sender, FormClosingEventArgs e)
        {
            proveedorseleccionado?.Invoke(this, EventArgs.Empty);
        }

        private void btnregresar_Click(object sender, EventArgs e)
        {
            proveedorseleccionado?.Invoke(this, EventArgs.Empty);
            this.Close();

        }

        public void inicampos()
        {
            btneliminar.Enabled = false;
            btnguardar.Visible = false;
            btnmodificar.Enabled = false;
            // btnImprimir.Enabled = false;
            btncancelar.Visible = false;
            txtruc.Enabled = false;
            txtproveedor.Enabled = false;
            txtdireccion.Enabled = false;
            txtcontacto.Enabled = false;
            txtemail.Enabled = false;
            txtciudad.Enabled = false;
            txtcelular.Enabled = false;
            txttelefono1.Enabled = false;
            txttelefono2.Enabled = false;
            txtnombre.Focus();
            btnguardarmodificar.Visible = false;
            txtcodigo.Enabled = false;
            txtgiro.Enabled = false;
            txtruta.Enabled = false;
            txtproductos.Enabled = false;
            txtnombre.Enabled = true;
            lbllistanombre.Enabled = true;
        }
        public void blockCampos()
        {
            txtruc.Enabled = false;
            txtproveedor.Enabled = false;
            txtdireccion.Enabled = false;
            txtcontacto.Enabled = false;
            txtemail.Enabled = false;
            txtciudad.Enabled = false;
            txtcelular.Enabled = false;
            txttelefono1.Enabled = false;
            txttelefono2.Enabled = false;
            txtcodigo.Enabled = false;
            txtgiro.Enabled = false;
            txtruta.Enabled = false;
            txtproductos.Enabled = false;
        }

        public void limpiarCampos()
        {
            txtruc.Text = "";
            txtproveedor.Text = "";
            txtdireccion.Text = "";
            txtcontacto.Text = "";
            txtemail.Text = "";
            txtciudad.Text = "";
            txtcelular.Text = "";
            txttelefono1.Text = "";
            txttelefono2.Text = "";
            txtid.Text = "";
            txtnombre.Text = "";
            txtcodigo.Text = "";
            txtgiro.Text = "";
            txtruta.Text = "";
            txtproductos.Text = "";
            btneliminar.Enabled = false;
            btnmodificar.Enabled = false;
            btnguardar.Visible = false;
            btnnuevo.Enabled = true;
            btnnuevo.Visible = true;
            btncancelar.Visible = false;
            txtnombre.Enabled = true;
            lbllistanombre.Enabled = true;

        }
        public void activarCampo()
        {
            txtruc.Enabled = true;
            txtproveedor.Enabled = true;
            txtdireccion.Enabled = true;
            txtcontacto.Enabled = true;
            txtemail.Enabled = true;
            txtciudad.Enabled = true;
            txtcelular.Enabled = true;
            txttelefono1.Enabled = true;
            txttelefono2.Enabled = true;
            txtcodigo.Enabled = true;
            txtgiro.Enabled = true;
            txtruta.Enabled = true;
            txtproductos.Enabled = true;
            // btnImprimir.Enabled = true;
            txtruc.Focus();

        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            limpiarCampos();
            txtnombre.Enabled = false;
            lbllistanombre.Enabled = false;
            long ultimoid;
            if (coleccionProveedores.Count == 0)
            {
                ultimoid = 1;
            }
            else
            {
                ultimoid = coleccionProveedores.Max(x => x.idproveedor) + 1;
            }
            txtid.Text = Convert.ToString(ultimoid);
            btnguardar.Visible = true;
            btnnuevo.Visible = false;
            txtruc.Focus();
            activarCampo();
            btncancelar.Visible = true;
            //btnImprimir.Enabled = false;
        }

        private void btncancelar_Click(object sender, EventArgs e)
        {
            limpiarCampos();
            inicampos();
            lbllistanombre.DataSource = null;
            lbllistanombre.DataSource = coleccionProveedores;
            lbllistanombre.DisplayMember = "proveedor";
            lbllistanombre.ValueMember = "idproveedor";
            //btneliminar.Enabled = true;
            btnregresar.Enabled = true;
        }

        private void btnguardarmodificar_Click(object sender, EventArgs e)
        {
            var prove = obtenerDatosForm();
            long id = prove.idproveedor;
            // ventana
            int posicion = coleccionProveedores.IndexOf(coleccionProveedores.SingleOrDefault(x => x.idproveedor == id));
            if (coleccionProveedores[posicion].ruc != prove.ruc || coleccionProveedores[posicion].proveedor != prove.proveedor ||
                coleccionProveedores[posicion].email != prove.email || coleccionProveedores[posicion].contacto != prove.contacto ||
                coleccionProveedores[posicion].direccion != prove.direccion || coleccionProveedores[posicion].celular != prove.celular
                || coleccionProveedores[posicion].telefono1 != prove.telefono1 || coleccionProveedores[posicion].telefono2 != prove.telefono2 ||
                coleccionProveedores[posicion].ciudad!=prove.ciudad||coleccionProveedores[posicion].giro != prove.giro||coleccionProveedores[posicion].ruta!=prove.ruta||
                coleccionProveedores[posicion].productos!=prove.productos || coleccionProveedores[posicion].codigo!=prove.codigo)
            {
                coleccionProveedores[posicion] = prove;

                // databse                        
                datpro.ModificarProveedor(prove);
               // limpiarCampos();
                blockCampos();
                btnguardarmodificar.Visible = false;
                btnregresar.Enabled = true;
                btncancelar.Visible = false;
                btnmodificar.Enabled = false;
                txtnombre.Enabled = true;
                lbllistanombre.Enabled = true;
                lbllistanombre.DataSource = null;
                lbllistanombre.DataSource = coleccionProveedores;
                lbllistanombre.DisplayMember = "proveedor";
                lbllistanombre.ValueMember = "idproveedor";
                txtnombre.Text = "";



            }
            else
            {
                DialogResult resul = MessageBox.Show("Debe modificar un dato", "Mensaje", MessageBoxButtons.OK);
                txtruc.Focus();
            }
        }

        public Proveedores obtenerDatosForm()
        {
            Proveedores pro = new Proveedores();
            pro.idproveedor = Convert.ToUInt32(txtid.Text);
            pro.proveedor= txtproveedor.Text;
            pro.contacto = txtcontacto.Text;
            pro.ruc = txtruc.Text;
            pro.direccion = txtdireccion.Text;
            pro.telefono1 = txttelefono1.Text;
            pro.telefono2 = txttelefono2.Text;
            pro.celular = txtcelular.Text;
            pro.email = txtemail.Text;
            pro.codigo = txtcodigo.Text;
            pro.ciudad = txtciudad.Text;
            pro.productos = txtproductos.Text;
            pro.ruta = txtruta.Text;
            pro.giro = txtgiro.Text;
            return pro;
        }

        public void obtenerdatos()
        {
            blockCampos();
            var cliSel = obtenerdatosid();
            txtid.Text = Convert.ToString(cliSel.idproveedor);
            txtproveedor.Text = cliSel.proveedor;
            txtdireccion.Text = cliSel.direccion;
            txtruc.Text = cliSel.ruc;
            txttelefono1.Text = cliSel.telefono1;
            txttelefono2.Text = cliSel.telefono2;
            txtruta.Text = Convert.ToString(cliSel.ruta);
            txtproductos.Text = Convert.ToString(cliSel.productos);
            txtemail.Text = cliSel.email;
            txtcelular.Text = cliSel.celular;
            txtcontacto.Text = cliSel.contacto;
            txtgiro.Text = cliSel.giro;
            txtciudad.Text = cliSel.ciudad;
            txtcodigo.Text = cliSel.codigo;
            btneliminar.Enabled = true;
            //btnnuevo.Enabled = false;
            btnmodificar.Enabled = true;
            btnnuevo.Visible = true;
            btnguardar.Visible = false;
            btncancelar.Visible = false;
           // btnImprimir.Enabled = true;
            // activarCampo();
        }

        public Proveedores obtenerdatosid()
        {
            if (lbllistanombre.SelectedValue == null)
            {
                MessageBox.Show("Seleccione un Cliente, por favor");
                return null;
            }
            int idSeleccionado = Convert.ToInt32(lbllistanombre.SelectedValue);
            var cliSel = coleccionProveedores.SingleOrDefault(x => x.idproveedor == idSeleccionado);
            if (cliSel == null)
            {
                return null;
            }
            return cliSel;
        }

        private void lbllistanombre_MouseClick(object sender, MouseEventArgs e)
        {
            obtenerdatos();
        }

        private void btnmodificar_Click(object sender, EventArgs e)
        {
            activarCampo();
            btnguardarmodificar.Visible = true;
            btncancelar.Visible = true;
            //btnImprimir.Enabled = false;
            btneliminar.Enabled = false;
            btnregresar.Enabled = false;
            txtnombre.Enabled = false;
            lbllistanombre.Enabled = false;
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                // DialogResult resul = MessageBox.Show("Seguro que quieres eliminar este Cliente? " + txtnombre.Text, "Eliminar Registro", MessageBoxButtons.YesNo);
                if (!txtnombre.Text.Equals(""))
                {
                    //DialogResult resul = MessageBox.Show("Seguro que quieres eliminar este Cliente? "+txtnombre.Text, "Eliminar Registro", MessageBoxButtons.YesNo);
                    var c = txtnombre.Text.ToLower();
                    var filcliente = datpro.filtrarProveedores(c);
                    //var resultados = (coleccionClientes.Where(x => x.cliente.StartsWith(c)));
                    //coleccionClientes.Clear();
                    coleccionresult.Clear();
                    foreach (var item in filcliente)
                    {
                        coleccionresult.Add(item);
                    }
                    lbllistanombre.DataSource = null;
                    lbllistanombre.DataSource = coleccionresult;
                    lbllistanombre.DisplayMember = "proveedor";
                    lbllistanombre.ValueMember = "idproveedor";
                    var t = txtnombre.Text;
                    if (coleccionresult != null && coleccionresult.Count == 1)
                    {
                        obtenerdatos();
                    }
                }
                else if (txtnombre.Text.Equals(""))
                {
                    lbllistanombre.DataSource = null;
                    lbllistanombre.DataSource = coleccionProveedores;
                    lbllistanombre.DisplayMember = "proveedor";
                    lbllistanombre.ValueMember = "idproveedor";
                    DialogResult resul = MessageBox.Show("Ingrese datos a buscar", "Mensaje", MessageBoxButtons.OK);
                }
            }
        }

        private void btnguardar_Click(object sender, EventArgs e)
        {

            if (validarCampos())
            {
                var cliente = obtenerDatosForm();
                long lastid = datpro.guardarproveedor(cliente);
                cliente.idproveedor = lastid;
                coleccionProveedores.Add(cliente);
                //limpiarCampos();
                blockCampos();
                btneliminar.Enabled = false;
                btnmodificar.Enabled = false;
                btnguardar.Visible = false;
                btnnuevo.Enabled = true;
                btnnuevo.Visible = true;
                btncancelar.Visible = false;
                txtnombre.Enabled = true;
                lbllistanombre.Enabled = true;

            }
        }

        public Boolean validarCampos()
        {
            Boolean valor = true;
            if (txtruc.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato RUC es obligatorio", "Validar", MessageBoxButtons.OK);
                txtruc.Focus();
                valor = false;
                return valor;
            }
            else if (txtproveedor.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Proveedor es obligatorio", "Validar", MessageBoxButtons.OK);
                txtproveedor.Focus();
                valor = false;
                return valor;
            }
            else if (txtcodigo.Text.Equals(""))
            {
                DialogResult resul = MessageBox.Show("El dato Codigo es obligatorio", "Validar", MessageBoxButtons.OK);
                txtcodigo.Focus();
                valor = false;
                return valor;
            }
            return valor;
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            DialogResult resul = MessageBox.Show("Seguro que quieres eliminar el proveedor " + txtproveedor.Text + "?", "Eliminar Registro", MessageBoxButtons.YesNo);
            if (resul == DialogResult.Yes)
            {
                var prod = obtenerdatosid();
                coleccionProveedores.Remove(prod);
                datpro.eliminarProveedor(prod.idproveedor);
                limpiarCampos();
            }
        }
    }

}



    
