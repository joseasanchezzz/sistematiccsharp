﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MClientes
{
   public class Compra
    {
        public long idfactura { get; set; }
        public string tipodoc { get; set; }
        public string documento { get; set; }
        public Nullable<long> idproveedor { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        //public string idvendedor { get; set; }
        public Nullable<double> subtotal { get; set; }
        public Nullable<double> igv { get; set; }
        public Nullable<double> total { get; set; }
        //public string estado { get; set; }
        //public string observacion { get; set; }
        //public string estado_can { get; set; }
        //public Nullable<double> inicial { get; set; }
        //public Nullable<double> saldo { get; set; }
        //public string moneda { get; set; }
        //public Nullable<decimal> cambio { get; set; }
        //public Nullable<double> descuento { get; set; }
        //public string condiciones { get; set; }
        //public Nullable<System.DateTime> fecha_vto { get; set; }
        //public string num_kardex { get; set; }
        //public string idcaja { get; set; }
        //public Nullable<int> local { get; set; }
        //public string ocompra { get; set; }
        //public string opedido { get; set; }
        //public Nullable<sbyte> anulada { get; set; }
        //public Nullable<decimal> percepcion { get; set; }
        //public string paga_con { get; set; }
        //public decimal detraccion { get; set; }
    }
}
